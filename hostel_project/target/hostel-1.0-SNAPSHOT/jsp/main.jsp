<%--
  Created by IntelliJ IDEA.
  User: NotePad.by
  Date: 14.08.2016
  Time: 14:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>--%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ctg" uri="customtags" %>
<%@ include file="/jsp/jspf/bundle.jspf"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="../img/pig_3983.ico" type="image/ico">
    <title><fmt:message key="title.main_page"/></title>

    <style>
        <%@include file="../css/bootstrap.css" %>
        <%@include file="../css/style.css" %>
    </style>

    <script type="text/javascript" src="<c:url value="../js/bootstrap.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="../js/jquery-3.1.0.min.js"/>"></script>
    <%--<script>--%>
        <%--<%@include file="../js/bootstrap.min.js"%>--%>
        <%--<%@include file="../js/jquery-3.1.0.min.js"%>--%>
    <%--</script>--%>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.1.0.min.js"></script>

</head>
<body>

<nav id="mainNav">
    <div class="container">
        <div class="row">
            <div class="navbar navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#resposive-menu" >
                            <span class="sr-only">Открыть навигацию</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="#" class="navbar-brand"> Hostel</a>
                    </div>
                    <div class="collapse navbar-collapse" id="resposive-menu">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#beds"><fmt:message key="nav.rooms"/></a></li>
                            <li>
                                <form class="form" method="POST" action="Controller">
                                    <input type="hidden" name="command" value="change_locale"  />
                                    <button class="locale" type="submit"><img src="../img/ru.png" alt="русский"></button>
                                    <input type="hidden" name="locale" value="ru"/>
                                </form>
                            </li>
                            <li>
                                <form class="form" method="POST" action="Controller">
                                    <input type="hidden" name="command" value="change_locale" />
                                    <button class="locale" type="submit"><img src="../img/en.png" alt="english"></button>
                                    <input type="hidden" name="locale" value="en"/>
                                </form>
                            </li>
                            <c:if test="${sessionScope.logged_user_id eq null}">
                                <li><button class="btn btn-success" type="button" data-toggle="modal" data-target="#modal-1"><fmt:message key="button.logIn"/></button></li>
                                <li><button class="btn btn-success" type="button" data-toggle="modal" data-target="#modal-2"><fmt:message key="button.signUp"/></button></li>
                            </c:if>
                            <c:if test="${sessionScope.logged_user_status eq true }">
                                <li>
                                <form action="Controller" method="post" >
                                    <input type="hidden" name="command" value="to_admin_page">
                                    <button class="btn btn-success" type="submit"><fmt:message key="button.my_page"/></button>
                                </form>
                                    </li>
                            </c:if>
                            <c:if test="${sessionScope.logged_user_status eq false }">
                                <li>
                                <form action="Controller" method="post" >
                                    <input type="hidden" name="command" value="to_user_page">
                                    <button class="btn btn-success" type="submit"><fmt:message key="button.my_page"/></button>
                                </form>
                                    </li>
                            </c:if>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </nav>
<%@include file="jspf/header.jspf"%>
<section id="beds">
    <div class="row">
        <h2><fmt:message key="header.rooms"/></h2>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="image">
                <h4><fmt:message key="static.six_bed"/></h4>
                <img src="../img/6.jpg" alt="6-местный номер" class="img-circle img-responsive">
            </div>
            <p><strong><fmt:message key="static.cost"/>:14.99</strong></p>
        </div>
        <div class="col-md-4">
            <div class="image">
                <h4><fmt:message key="static.three_bed"/></h4>
                <img src="../img/3.jpg" alt="3-местный номер" class="img-circle img-responsive">
            </div>
            <p><strong><fmt:message key="static.cost"/>:17.99</strong></p>
        </div>
        <div class="col-md-4">
            <div class="image">
                <h4><fmt:message key="static.two_bed"/></h4>
                <img src="../img/2.jpg" alt="2-местный номер" class="img-circle img-responsive">
            </div>
            <p><strong><fmt:message key="static.cost"/>:19.99</strong></p>
        </div>
    </div>
</section>
<section id="feedback">

</section>
<div class="modal fade" id="modal-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal">
                    &times;
                </button>
                <h2 class="modal-title">\<fmt:message key="static.log_in"/></h2>
            </div>
            <div class="modal-body">
                <form method="post" action="Controller">
                    <input type="hidden" name="command" value="log_in">
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="Email"><fmt:message key="static.email"/>:</label>
                        <div class="col-xs-9">
                            <input type="email" class="form-control" id="Email" name ="email" placeholder="Email"pattern="(\w{6,})@(\w+\.)([a-z]{2,4})" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="Password"><fmt:message key="static.password"/>:</label>
                        <div class="col-xs-9">
                            <input type="password" class="form-control" id="Password" name="password" placeholder="<fmt:message key="static.enter_password"/>" pattern="[\w]{4,30}" required>
                            <%----%>
                        </div>
                    </div>
                    <button class="btn btn-primary" type="submit">
                        <fmt:message key="button.enter"/>
                    </button>
                </form>
            </div>
            <div class="modal-footer">

                <button class="btn btn-danger" type="button" data-dismiss="modal"><fmt:message key="button.cancel"/></button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-2">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal">
                    &times;
                </button>
                <h2><fmt:message key="static.registration"/></h2>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="Controller" method="post">
                    <input type="hidden" name="command" value="register">
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="lastName"><fmt:message key="static.lastName"/>:</label>
                        <div class="col-xs-9">
                            <input type="text" class="form-control" id="lastName" name="lastName" placeholder="<fmt:message key="static.enter_lastName"/>" pattern="[А-Яа-яA-Za-z]{2,30}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="firstName" ><fmt:message key="static.firstName"/>:</label>
                        <div class="col-xs-9">
                            <input type="text" class="form-control" id="firstName" name="firstName" placeholder="<fmt:message key="static.enter_firstName"/>" pattern="[А-Яа-яA-Za-z]{2,30}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="inputEmail"><fmt:message key="static.email"/>:</label>
                        <div class="col-xs-9">
                            <input type="email" class="form-control" id="inputEmail" name ="email" placeholder="Email" pattern="(\w{6,})@(\w+\.)([a-z]{2,4})" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="inputPassword"><fmt:message key="static.password"/>:</label>
                        <div class="col-xs-9">
                            <input type="password" class="form-control" id="inputPassword" name="password" placeholder="<fmt:message key="static.enter_password"/>" pattern="[\w]{3,30}" required>
                        </div>
                    </div>
                    <br />
                    <div class="form-group">
                        <div class="col-xs-offset-3 col-xs-9">
                            <input type="submit" class="btn btn-primary" value="<fmt:message key="button.register"/>">
                            <input type="reset" class="btn btn-default" value="<fmt:message key="button.reset"/>">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" type="button" data-dismiss="modal"><fmt:message key="button.cancel"/></button>
            </div>
        </div>
    </div>
</div>
<%@include file="jspf/footer.jspf"%>
</body>
</html>