<%--
  Created by IntelliJ IDEA.
  User: NotePad.by
  Date: 14.08.2016
  Time: 23:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>--%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="/jsp/jspf/bundle.jspf"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><fmt:message key="title.admin_page"/></title>

    <script type="text/javascript" src="<c:url value="../js/bootstrap.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="../js/jquery-3.1.0.min.js"/>"></script>
    <script>
        <%@include file="../js/bootstrap.min.js"%>
        <%@include file="../js/jquery-3.1.0.min.js"%>
    </script>
    <style>
        <%@include file="../css/bootstrap.css" %>
        <%@include file="../css/style.css" %>
    </style>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.1.0.min.js"></script>
</head>
<body>
<nav id="mainNav">
    <div class="container">
        <div class="row">
            <div class="navbar navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#resposive-menu" >
                            <span class="sr-only">Открыть навигацию</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="main.jsp" class="navbar-brand"> Hostel</a>
                    </div>
                    <div class="collapse navbar-collapse" id="resposive-menu">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#orders"><fmt:message key="nav.orders"/></a></li>
                            <li><a href="#userses"><fmt:message key="nav.users"/></a></li>
                            <li>
                                <form class="form" method="POST" action="Controller">
                                    <input type="hidden" name="command" value="change_locale"  />
                                    <button class="locale" type="submit"><img src="../img/ru.png" alt="русский"></button>
                                    <input type="hidden" name="locale" value="ru"/>
                                </form>
                            </li>
                            <li>
                                <form class="form" method="POST" action="Controller">
                                    <input type="hidden" name="command" value="change_locale" />
                                    <button class="locale" type="submit"><img src="../img/en.png" alt="english"></button>
                                    <input type="hidden" name="locale" value="en"/>
                                </form>
                            </li>
                            <li>
                            <form action="Controller" method="post">
                                <input type="hidden" name="command" value="log_out">
                                <button class="btn btn-danger" type="submit"><fmt:message key="button.log_out"/></button>
                            </form>
                                </li>
                            <li>
                            <form action="Controller" method="post">
                                <input type="hidden" name="command" value="to_main_page">
                                <button class="btn btn-default" type="submit"><fmt:message key="button.main_page"/> </button>
                            </form>
                            </ul>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</nav>
<%@include file="jspf/header.jspf"%>
<section id="orders">
    <h3><fmt:message key="header.orders"/></h3>
    <button class="btn btn-success" type="button" data-toggle="modal" data-target="#modal-1"><fmt:message key="button.create_order"/></button>
    <br>
    <div class="modal fade" id="modal-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal">
                        &times;
                    </button>
                    <h2><fmt:message key="header.create_order"/></h2>
                </div>
                <div class="modal-body">
                    <form method="post" id="bookform"action="Controller">
                        <input type="hidden" name="command" value="make_order"/>
                        <h4>
                            <fmt:message key="header.choose_date"/>
                        </h4>
                        <div class="step_1" id="step_1" >
                            <fmt:message key="header.choose_arrival_date"/>
                            <input type="date" name="date_arrival">
                            <fmt:message key="header.choose_departure_date"/>
                            <input type="date" name="date_departure">
                        </div>
                        <h4><fmt:message key="header.choose_place"/></h4>
                        <div class="step_2" id="step_2">
                            <table class="table table-striped">
                                <thead>
                                <tr class="head">
                                    <td class="room"><fmt:message key="static.type_of_room"/> </td>
                                    <td class="date"><fmt:message key="static.price_and_free_rooms"/></td>
                                    <td class="date"><fmt:message key="static.number_of_ordered_rooms"/></td>
                                    <td></td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="room">
                                        <b><fmt:message key="static.six_bed"/></b>
                                    </td>
                                    <td class="date date-3">
                                        <fmt:message key="static.order_price"/>: <br>
                                        <span><fmt:message key="static.free"/>: ${free_six_beds}</span><br><label>
                                    </label></td>
                                    <td class="date date-3">
                                        <input type="number" name="amount_of_six_rooms" min="0" max=${free_six_beds}>
                                    </td>
                                </tr>
                                <tr class="gray">
                                    <td class="room">
                                        <b><fmt:message key="static.three_bed"/></b>
                                    </td>
                                    <td class="date date-4">
                                        <fmt:message key="static.order_price"/>: <br>
                                        <span><fmt:message key="static.free"/>: ${free_three_beds}</span><br>
                                    </td>
                                    <td class="date date-4">
                                        <input type="number" name="amount_of_three_rooms"  min="0"  max=${free_three_beds}>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="room">
                                        <b><fmt:message key="static.two_bed"/></b>
                                    </td>
                                    <td class="date date-5">
                                        <fmt:message key="static.order_price"/>: <br>
                                        <span><fmt:message key="static.free"/>: ${free_two_beds}</span><br>
                                    <td class="date date-5">
                                        <input type="number" name="amount_of_two_rooms" min="0"  max=${free_two_beds}>
                                </tr>
                                </tbody>
                            </table>
                            <button class="btn btn-primary" type="submit"><fmt:message key="button.make_order"/></button>
                        </div>
                    </form>

                </div>

            </div>

        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr class="head">
                <th><fmt:message key="static.order_number"/></th>
                <th><fmt:message key="static.user_id"/></th>
                <th><fmt:message key="static.date_arrival"/></th>
                <th><fmt:message key="static.date_departure"/></th>
                <th><fmt:message key="static.place_number"/></th>
                <th><fmt:message key="static.order_status"/></th>
                <th><fmt:message key="static.payment_status"/></th>
                <th><fmt:message key="static.price"/></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
                <c:forEach var="order" items="${orders}">
                    <c:if test="${order.getClass().getSimpleName() eq 'Order'}">
                        <tr>
                            <td ><c:out value="${order.getId()}"/></td>
                            <td><c:out value="${order.getUsersID()}"/></td>
                            <td><c:out value="${order.getArrival()}"/></td>
                            <td><c:out value="${order.getDeparture()}"/></td>
                            <td>
                                <c:forEach var="place" items="${order.getPlaceID()}">
                                    <c:out value="${place}"/>
                                    <c:out value=" "/>
                                </c:forEach>
                            </td>
                            <td>
                                <c:if test="${order.getOrderStatus() eq 'cancelled'}">
                                    <fmt:message key="static.cancelled"/>
                                </c:if>
                                <c:if test="${order.getOrderStatus() eq 'confirmed'}">
                                    <fmt:message key="static.confirmed"/>
                                </c:if>
                                <c:if test="${order.getOrderStatus() eq 'processing'}">
                                    <fmt:message key="static.processing"/>
                                </c:if>
                            </td>
                            <td>
                                <c:if test="${order.getPaymentStatus() eq 'booked'}">
                                    <fmt:message key="static.booked"/>
                                </c:if>
                                <c:if test="${order.getPaymentStatus() eq 'fully_payed'}">
                                    <fmt:message key="static.fully_payed"/>
                                </c:if>
                            </td>
                            <td><c:out value="${order.getPrice()}"/></td>
                            <td>
                                <c:if test="${order.getOrderStatus() eq 'processing'}">
                                        <form action="Controller" method="post">
                                            <input type="hidden" name="command" value="change_order_status">
                                            <input type="hidden" name="order_status" value="cancelled">
                                            <input type="hidden" name="order_id" value="${order.getId()}" >
                                            <button class="btn btn-danger"><fmt:message key="button.cancel"/></button>
                                        </form>
                                        <form action="Controller" method="post">
                                            <input type="hidden" name="command" value="change_order_status">
                                            <input type="hidden" name="order_status" value="confirmed">
                                            <input type="hidden" name="order_id" value="${order.getId()}" >
                                            <button class="btn btn-success"><fmt:message key="button.confirm"/></button>
                                        </form>
                                </c:if>
                            </td>
                        </tr>
                    </c:if>
                </c:forEach>
            </tbody>
        </table>
        <table class="pagination" border="1" cellpadding="10" cellspacing="10">
            <tr>
                <c:forEach begin="1" end="${noOfPages}" var="i">
                    <c:choose>
                        <c:when test="${currentPage eq i}">
                            <td>${i}</td>
                        </c:when>
                        <c:otherwise>
                            <td><a href="Controller?page=${i}">${i}</a></td>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </tr>
        </table>
    </div>
</section>
<section id="userses">
    <div class="table-responsive">
        <h3><fmt:message key="header.users"/> </h3>
        <table class="table table-striped">
            <tr class="head">
                <th>ID</th>
                <th>E-mail</th>
                <th><fmt:message key="static.firstName"/></th>
                <th><fmt:message key="static.lastName"/></th>
                <th><fmt:message key="static.visits"/></th>
                <th><fmt:message key="static.user_state"/></th>
                <th></th>
            </tr>
            <c:forEach var="user" items="${users}">
                <c:if test="${user.getClass().getSimpleName() eq 'User'}">
                    <tr>
                        <td><c:out value="${user.getId()}"/></td>
                        <td><c:out value="${user.getEmail()}"/></td>
                        <td><c:out value="${user.getName()}"/></td>
                        <td><c:out value="${user.getLastName()}"/></td>
                        <td><c:out value="${user.getVisits()}"/></td>
                        <td>
                            <c:if test="${user.isActive() eq true}">
                                <fmt:message key="static.user_state_active"/>
                            </c:if>
                            <c:if test="${user.isActive() eq false}">
                                    <fmt:message key="static.user_state_blocked"/>
                            </c:if>
                        <td>
                            <c:if test="${user.isActive() eq true}">
                                <form action="Controller" method="post">
                                    <input type="hidden" name="command" value="block_user">
                                    <input type="hidden" name="email" value="${user.getEmail()}">
                                    <input type="hidden" name="status" value="${user.isActive()}">
                                    <button class="btn btn-danger" type="submit"><fmt:message key="button.block"/></button>
                                </form>
                            </c:if>
                            <c:if test="${user.isActive() eq false}">
                                <form action="Controller" method="post">
                                    <input type="hidden" name="command" value="block_user">
                                    <input type="hidden" name="status" value="${user.isActive()}">
                                    <input type="hidden" name="email" value="${user.getEmail()}">
                                    <button class="btn btn-success" type="submit"><fmt:message key="button.unblock"/></button>
                                </form>
                            </c:if>
                        </td>
                    </tr>
                </c:if>
            </c:forEach>
        </table>
    </div>
</section>
<%@include file="jspf/footer.jspf"%>
</body>
</html>
