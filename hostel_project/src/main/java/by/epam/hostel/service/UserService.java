package by.epam.hostel.service;

import by.epam.hostel.dao.UserDAO;
import by.epam.hostel.dao.impl.UserDAOImpl;
import by.epam.hostel.entity.User;
import by.epam.hostel.exception.DAOException;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NotePad.by on 21.09.2016.
 */
public class UserService {
    Logger log = Logger.getLogger(UserService.class);

    private UserDAO userDAO = UserDAOImpl.getInstance();
    private static UserService instance = new UserService();

    public static UserService getInstance() {
        return instance;
    }

    public boolean signUp(User user){
        boolean result = false;
        try {
           result= userDAO.signUp(user);
        } catch (DAOException e) {
            log.error(e);
        }
        return result;
    }

    public boolean isExist(String email){
        boolean result = false;
        try {
            result = userDAO.isExist(email);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return result;
    }
    public User findEntityById(Integer id){
        User user = null;
        try {
            user = userDAO.findEntityById(id);
        } catch (DAOException e) {
            log.error(e);
        }
        return user;
    }

    public boolean changeVisits(User user){
        boolean result = false;
        result = userDAO.changeVisits(user);
        try {
            DiscountService.getInstance().setUserDiscount(user);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean changeStatus(String email, boolean status){
        boolean result = false;
        try {
            result = userDAO.changeStatus(email, status);
        } catch (DAOException e) {
            log.error(e);
        }
        return result;
    }

    public User findUserByEmail(String email){
        User user = null;
        try {
            user = userDAO.findUserByEmail(email);
        } catch (DAOException e) {
            log.error(e);
        }
        return user;
    }

    public List<User> findUserByRole(boolean isAdmin){
        List<User> users = new ArrayList<>();
        try {
            users = userDAO.findUserByRole(isAdmin);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return users;
    }
}
