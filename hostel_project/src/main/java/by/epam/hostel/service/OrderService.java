package by.epam.hostel.service;

import by.epam.hostel.dao.OrderDAO;
import by.epam.hostel.dao.impl.OrderDAOImpl;
import by.epam.hostel.entity.Order;
import by.epam.hostel.entity.Place;
import by.epam.hostel.entity.User;
import by.epam.hostel.entity.enumuration.OrderStatus;
import by.epam.hostel.entity.enumuration.PaymentStatus;
import by.epam.hostel.entity.enumuration.TypeOfRoom;
import by.epam.hostel.exception.DAOException;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrderService {
    Logger log = Logger.getLogger(OrderService.class);
    private static OrderService instance = new OrderService();
    private OrderDAO orderDAO = OrderDAOImpl.getInstance();

    public static OrderService getInstance() {
        return instance;
    }

    public int getNoOfRecords(){
        return orderDAO.getNoOfRecords();
    }
    public void MakeOrder(int[]amountOfRooms, Order order, boolean isAdmin) throws DAOException {
        log.info("OrderService.userMakeOrder");
        PlaceService placeService = PlaceService.getInstance();
        UserService userService = UserService.getInstance();
        order.setId(order.hashCode());
        DiscountService.getInstance().setOrderDiscount(order);
        float userDiscount = 0;
        if (!isAdmin) {
            User user = userService.findEntityById(order.getUsersID());
            userService.changeVisits(user);
            userDiscount = user.getDiscount();
        } else {
            order.setPaymentStatus(PaymentStatus.fully_payed);
        }
        float discount = order.getDiscount() + userDiscount;
            for (int j = 0; j < amountOfRooms.length; j++) {
                for(int i = 0; i < amountOfRooms[j]; i++){
                    Place place;
                    place = placeService.findPlaceByType(TypeOfRoom.findByAmount(j)).get(0);
                    placeService.changePlaceStatus(place.getId(), false);
                    order.getPlaceID().add(place.getId());
                }
            }
        order.setOrderDate(new Date());
        order.setPrice(placeService.countOrderPrice(order.getPlaceID(),discount));
        orderDAO.createOrder(order);
        DiscountService.getInstance().setOrderDaoDiscount(order);
    }
    public List<Order> pageViewAll(int offset, int noOfRecords){
       return orderDAO.pageViewAll(offset, noOfRecords);
    }
    public boolean changeOrderStatus(int orderId,OrderStatus orderStatus){
       boolean result = orderDAO.changeOrderStatus(orderId,orderStatus);
        return result;
    }
}
