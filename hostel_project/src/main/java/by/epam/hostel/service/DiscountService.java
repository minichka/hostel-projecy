package by.epam.hostel.service;

import by.epam.hostel.dao.DiscountDAO;
import by.epam.hostel.dao.OrderDAO;
import by.epam.hostel.dao.UserDAO;
import by.epam.hostel.dao.impl.DiscountDAOImpl;
import by.epam.hostel.dao.impl.OrderDAOImpl;
import by.epam.hostel.dao.impl.UserDAOImpl;
import by.epam.hostel.entity.Order;
import by.epam.hostel.entity.User;
import by.epam.hostel.entity.enumuration.TypeOfDiscount;
import by.epam.hostel.exception.DAOException;
import org.apache.log4j.Logger;

import java.util.Date;

/**
 * Created by NotePad.by on 13.09.2016.
 */
public class DiscountService {
    Logger log = Logger.getLogger(DiscountService.class);

    private DiscountDAO discountDAO = DiscountDAOImpl.getInstance();
    private static DiscountService instance = new DiscountService();

    public static DiscountService getInstance() {
        return instance;
    }

    public void setUserDiscount(User user) throws DAOException {
        log.info("getUserDiscount");
        UserDAO userDAO = UserDAOImpl.getInstance();
        log.info(user.getVisits());
        if ( user.getVisits() == 0){
            userDAO.updateDiscount(user.getId(), TypeOfDiscount.NEW_USER);
            user.setDiscount(discountDAO.findDiscountByType(TypeOfDiscount.NEW_USER).getDiscount());
        }
        else if(user.getVisits() >= 2 && user.getVisits() < 5){
            userDAO.updateDiscount(user.getId(), TypeOfDiscount.TWO_VISITS);
            user.setDiscount(discountDAO.findDiscountByType(TypeOfDiscount.TWO_VISITS).getDiscount());
            log.info(user.getDiscount());
        }
        else if(user.getVisits() >= 5 && user.getVisits() < 10){
            userDAO.updateDiscount(user.getId(), TypeOfDiscount.FIVE_VISITS);
            user.setDiscount(discountDAO.findDiscountByType(TypeOfDiscount.FIVE_VISITS).getDiscount());
            log.info(user.getDiscount());
        }
        else if(user.getVisits() >= 10) {
            userDAO.updateDiscount(user.getId(), TypeOfDiscount.TEN_VISITS);
            user.setDiscount(discountDAO.findDiscountByType(TypeOfDiscount.TEN_VISITS).getDiscount());
            log.info(user.getDiscount());
        }

    }
    public  void setOrderDiscount(Order order) throws DAOException {
        log.info("setOrderDiscount");
        DiscountDAO discountDAO = DiscountDAOImpl.getInstance();
        Date arrival = order.getArrival();
        Date departure = order.getDeparture();
        long diff = departure.getTime() - arrival.getTime();
        long days = diff/(24 * 60 * 60 * 1000);
        log.info(days);
        if(days >= 3 && days < 5) {
            order.setDiscount(discountDAO.findDiscountByType(TypeOfDiscount.THREE_DAYS).getDiscount());
        }
        else if (days >= 5 && days < 10 ) {
            order.setDiscount(discountDAO.findDiscountByType(TypeOfDiscount.FIVE_DAYS).getDiscount());
        }
        else if (days >= 10) {
            order.setDiscount(discountDAO.findDiscountByType(TypeOfDiscount.TEN_DAYS).getDiscount());

        }
    }
    public  void setOrderDaoDiscount(Order order) throws DAOException {
        log.info("setOrderDaoDiscount");
        OrderDAO orderDAO = OrderDAOImpl.getInstance();
        Date arrival = order.getArrival();
        Date departure = order.getDeparture();
        long diff = departure.getTime() - arrival.getTime();
        long days = diff/(24 * 60 * 60 * 1000);
        if(days >= 3 && days < 5) {
            orderDAO.updateDiscount(order.getId(), TypeOfDiscount.THREE_DAYS);
        }
        else if (days >= 5 && days < 10 ) {
            orderDAO.updateDiscount(order.getId(), TypeOfDiscount.FIVE_DAYS);
        }
        else if (days >= 10) {
            orderDAO.updateDiscount(order.getId(), TypeOfDiscount.TEN_DAYS);

        }

    }
}
