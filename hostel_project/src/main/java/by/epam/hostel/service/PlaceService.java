package by.epam.hostel.service;

import by.epam.hostel.dao.PlaceDAO;
import by.epam.hostel.dao.impl.PlaceDAOImpl;
import by.epam.hostel.entity.Place;
import by.epam.hostel.entity.enumuration.TypeOfRoom;
import by.epam.hostel.exception.DAOException;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NotePad.by on 21.09.2016.
 */
public class PlaceService {
    Logger log = Logger.getLogger(PlaceService.class);

    private PlaceDAO placeDAO = PlaceDAOImpl.getInstance();
    private static PlaceService instance = new PlaceService();

    public static PlaceService getInstance() {
        return instance;
    }

    public boolean changePlaceStatus(int id, boolean status){
        log.info("PlaceService.changePlaceStatus");
        boolean result = false;
        try {
            result = placeDAO.changePlaceStatus(id, status);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public float countOrderPrice(List<Integer> places, float discount){
        float sum = 0;
        for (int i : places){
            try {
                Place place = placeDAO.findEntityById(i);
                sum += place.getPrice();
            } catch (DAOException e) {
                e.printStackTrace();
            }
        }
        sum -= sum*discount;

        return sum;
    }
    public List<Place> findPlaceByType(TypeOfRoom typeOfRoom){
        log.info("PlaceService.findPlaceByType");
        List<Place> places = new ArrayList<>();
        try {
            places = placeDAO.findPlaceByType(typeOfRoom);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return places;
    }

    public int countFreeRooms(TypeOfRoom typeOfRoom, boolean isFree){
        int count = 0;
        try {
            count = placeDAO.countFreeRooms(typeOfRoom,isFree);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        return count;
    }
}
