package by.epam.hostel.dao.impl;

import by.epam.hostel.dao.UserDAO;
import by.epam.hostel.dao.pool.DBConnectionPool;
import by.epam.hostel.entity.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import by.epam.hostel.entity.enumuration.TypeOfDiscount;
import by.epam.hostel.exception.ConnectionPoolException;
import by.epam.hostel.exception.DAOException;

import org.apache.log4j.Logger;

public class UserDAOImpl implements UserDAO {
    private static final Logger log = Logger.getLogger(UserDAOImpl.class);

    private static UserDAOImpl instance = new UserDAOImpl();
    private static final String USER_ID="users.iduser";
    private static final String USER_DISCOUNT="users.discount";
    private static final String USER_EMAIL="users.email";
    private static final String USER_PASSWORD="users.password";
    private static final String USER_IS_ACTIVE="users.isActive";
    private static final String USER_VISITS="users.visits";
    private static final String USER_IS_ADMIN="users.isAdmin";
    private static final String USER_NAME = "users.Name";
    private static final String USER_LASTNAME = "users.Last_name";

    private static final String SQL_SELECT_ALL_USERS=" SELECT * FROM hostel_project.users";
    private static final String SQL_SELECT_USERS_BY_ID=" SELECT * FROM hostel_project.users WHERE iduser=?";
    private static final String SQL_SELECT_USERS_BY_STATUS=" SELECT * FROM hostel_project.users WHERE isActive=?";
    private static final String SQL_SIGNUP_USER=" INSERT INTO hostel_project.users(iduser,discount,email,password,isActive,visits,isAdmin,Name,Last_name) VALUES(?,?,?,?,?,?,?,?,?)";
    private static final String SQL_IS_USER_EXISTS="SELECT * FROM hostel_project.users WHERE users.email=?";
    private static final String SQL_DELETE_USER="DELETE FROM hostel_project.users WHERE users.iduser=?";
    private static final String SQL_SELECT_USERS_BY_ROLE=" SELECT* FROM hostel_project.users WHERE isAdmin=?";
    private static final String SQL_SELECT_USERS_BY_EMAIL_AND_PASSWORD=" SELECT * FROM hostel_project.users WHERE email=?";
    private static final String SQL_UPDATE_USER_STATUS=" UPDATE hostel_project.users SET isActive=? WHERE users.email=?";
    private static final String SQL_UPDATE_USER_VISITS= "UPDATE hostel_project.users SET visits=? WHERE users.iduser=?";
    private static final String SQL_UPDATE_USER_DISCOUNT= "UPDATE hostel_project.users SET discount=? WHERE users.iduser=?";

    public static UserDAOImpl getInstance() {
        return instance;
    }

    private int noOfRecords;

    public int getNoOfRecords() {
        return noOfRecords;
    }

    public boolean delete(int id) throws DAOException {
        DBConnectionPool pool = DBConnectionPool.getInstance();

        boolean result = false;
        Connection connection = null;
        PreparedStatement prStatement = null;
        try {
            connection = pool.takeConnection();
            prStatement = connection.prepareStatement(SQL_DELETE_USER);
            prStatement.setInt(1, id);
            if (prStatement.executeUpdate() > 0) {
                result = true;
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (prStatement != null) {
                    prStatement.close();
                }
                pool.closeConnection(connection);
            } catch (SQLException | ConnectionPoolException e) {
                throw new DAOException(e);
            }
        }
        return result;
    }
    @Override
    public boolean isExist(String email) throws DAOException {
        log.info("UserDAO.isExist()");
        DBConnectionPool pool = DBConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        boolean result;
        try {
            connection = pool.takeConnection();
            preparedStatement = connection.prepareStatement(SQL_IS_USER_EXISTS);
            preparedStatement.setString(1,email);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = resultSet.first();
        } catch (ConnectionPoolException|SQLException e) {
            log.info("IsExist Error");
            throw new DAOException(e);
        } finally {
            try {
                if (preparedStatement != null)
                    preparedStatement.close();
                pool.closeConnection(connection);
            } catch (SQLException|ConnectionPoolException e) {
                log.error(e);
            }
        }
        return result;
    }

    @Override
    public List<User> findAll() throws DAOException {
        return null;
    }

    @Override
    public boolean signUp(User user) throws DAOException {
        log.info("UserDAO.signUp()");
        DBConnectionPool pool = DBConnectionPool.getInstance();
        boolean result = false;
        if (user == null || isExist(user.getEmail()))
            return result;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = pool.takeConnection();
            preparedStatement = connection.prepareStatement(SQL_SIGNUP_USER);
            preparedStatement.setInt(1, user.getId());
            preparedStatement.setInt(2, DiscountDAOImpl.getInstance().findDiscountByType(TypeOfDiscount.NEW_USER).getId());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setBoolean(5, user.isActive());
            preparedStatement.setInt(6, user.getVisits());
            preparedStatement.setBoolean(7, user.isAdmin());
            preparedStatement.setString(8, user.getName());
            preparedStatement.setString(9, user.getLastName());
            if (preparedStatement.executeUpdate() > 0)
                result = true;

        } catch (ConnectionPoolException|SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                if (preparedStatement != null)
                    preparedStatement.close();
                pool.closeConnection(connection);
            } catch (SQLException|ConnectionPoolException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @Override
    public User findUserByEmail(String email) throws DAOException {
        DBConnectionPool pool = DBConnectionPool.getInstance();
        User user = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = pool.takeConnection();
            preparedStatement = connection.prepareStatement(SQL_SELECT_USERS_BY_EMAIL_AND_PASSWORD);
            preparedStatement.setString(1,email);
//            preparedStatement.setString(2,password);
            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                user = new User();
                user.setId(resultSet.getInt(USER_ID));
                user.setDiscount(DiscountDAOImpl.getInstance().findEntityById(resultSet.getInt(USER_DISCOUNT)).getDiscount());
                user.setEmail(resultSet.getString(USER_EMAIL));
                user.setPassword(resultSet.getString(USER_PASSWORD));
                user.setActive(resultSet.getBoolean(USER_IS_ACTIVE));
                user.setVisits(resultSet.getInt(USER_VISITS));
                user.setAdmin(resultSet.getBoolean(USER_IS_ADMIN));
                user.setName(resultSet.getString(USER_NAME));
                user.setLastName(resultSet.getString(USER_LASTNAME));
            }
        } catch (ConnectionPoolException|SQLException e) {
            log.error(e);
            throw  new DAOException(e);
        }finally {
            try {
                if(preparedStatement != null)
                    preparedStatement.close();
                pool.closeConnection(connection);
            } catch (SQLException|ConnectionPoolException e) {
                log.error(e);
            }
        }
        return user;
    }

    @Override
    public User findEntityById(Integer id) throws DAOException {
        DBConnectionPool pool = DBConnectionPool.getInstance();
        User user = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = pool.takeConnection();
            preparedStatement = connection.prepareStatement(SQL_SELECT_USERS_BY_ID);
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                user = new User();
                user.setId(resultSet.getInt(USER_ID));
                user.setDiscount(DiscountDAOImpl.getInstance().findEntityById(resultSet.getInt(USER_DISCOUNT)).getDiscount());
                user.setEmail(resultSet.getString(USER_EMAIL));
                user.setPassword(resultSet.getString(USER_PASSWORD));
                user.setActive(resultSet.getBoolean(USER_IS_ACTIVE));
                user.setVisits(resultSet.getInt(USER_VISITS));
                user.setAdmin(resultSet.getBoolean(USER_IS_ADMIN));
                user.setName(resultSet.getString(USER_NAME));
                user.setLastName(resultSet.getString(USER_LASTNAME));
            }
        } catch (ConnectionPoolException|SQLException e) {
            log.error(e);
            throw  new DAOException(e);
        }finally {
            try {
                if(preparedStatement != null)
                    preparedStatement.close();
                pool.closeConnection(connection);
            } catch (SQLException|ConnectionPoolException e) {
                log.error(e);
            }
        }
        return user;
    }

    @Override
    public boolean updateDiscount(int id, TypeOfDiscount typeOfDiscount) throws DAOException {
        log.info("updateDiscount");
        DBConnectionPool pool = DBConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        boolean result = false;
        try {
            connection = pool.takeConnection();
            preparedStatement = connection.prepareStatement(SQL_UPDATE_USER_DISCOUNT);
            preparedStatement.setInt(1,DiscountDAOImpl.getInstance().findDiscountByType(typeOfDiscount).getId());
            preparedStatement.setInt(2,id);
            if(preparedStatement.executeUpdate() > 0)
                result = true;
        } catch (ConnectionPoolException|SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                if (preparedStatement != null)
                    preparedStatement.close();
                pool.closeConnection(connection);

            } catch (ConnectionPoolException|SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @Override
    public List<User> findUserByStatus(boolean status) throws DAOException {
        log.info("findUserByStatus");
        DBConnectionPool pool = DBConnectionPool.getInstance();
        ArrayList<User> userses = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = pool.takeConnection();
            preparedStatement = connection.prepareStatement(SQL_SELECT_USERS_BY_STATUS);
            preparedStatement.setBoolean(1,status);
            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                User user = new User();
                user.setId(resultSet.getInt(USER_ID));
                user.setDiscount(resultSet.getInt(USER_DISCOUNT));
                user.setEmail(resultSet.getString(USER_EMAIL));
                user.setPassword(resultSet.getString(USER_PASSWORD));
                user.setActive(resultSet.getBoolean(USER_IS_ACTIVE));
                user.setVisits(resultSet.getInt(USER_VISITS));
                user.setAdmin(resultSet.getBoolean(USER_IS_ADMIN));
                user.setName(resultSet.getString(USER_NAME));
                user.setName(resultSet.getString(USER_LASTNAME));
                userses.add(user);
            }
        } catch (ConnectionPoolException|SQLException e) {
            log.error(e);
            throw  new DAOException(e);
        }finally {
            try {
                if(preparedStatement != null)
                    preparedStatement.close();
                pool.closeConnection(connection);
            } catch (SQLException|ConnectionPoolException e) {
                log.error(e);
            }
        }
        return userses;
    }

    @Override
    public List<User> findUserByRole(boolean isAdmin) throws DAOException {
        log.info("findUserByRole");
        DBConnectionPool pool = DBConnectionPool.getInstance();
        ArrayList<User> userses = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = pool.takeConnection();
            preparedStatement = connection.prepareStatement(SQL_SELECT_USERS_BY_ROLE);
            preparedStatement.setBoolean(1,isAdmin);
            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                User user = new User();
                user.setId(resultSet.getInt(USER_ID));
                user.setDiscount(resultSet.getInt(USER_DISCOUNT));
                user.setEmail(resultSet.getString(USER_EMAIL));
                user.setPassword(resultSet.getString(USER_PASSWORD));
                user.setActive(resultSet.getBoolean(USER_IS_ACTIVE));
                user.setVisits(resultSet.getInt(USER_VISITS));
                user.setAdmin(resultSet.getBoolean(USER_IS_ADMIN));
                user.setName(resultSet.getString(USER_NAME));
                user.setName(resultSet.getString(USER_LASTNAME));
                userses.add(user);
            }
        } catch (ConnectionPoolException|SQLException e) {
            log.error(e);
            throw  new DAOException(e);
        }
        finally {
            try {
                if(preparedStatement != null)
                    preparedStatement.close();
                pool.closeConnection(connection);
            } catch (SQLException|ConnectionPoolException e) {
                log.error(e);
            }
        }
        return userses;
    }

    @Override
    public boolean changeStatus(String email, boolean status) throws DAOException {
        log.info("changeStatus");
        DBConnectionPool pool = DBConnectionPool.getInstance();
        boolean result = false;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = pool.takeConnection();
            preparedStatement = connection.prepareStatement(SQL_UPDATE_USER_STATUS);
            preparedStatement.setBoolean(1,status);
            preparedStatement.setString(2,email);
            if (preparedStatement.executeUpdate() == 1)
                result = true;
        } catch (ConnectionPoolException|SQLException e) {
            log.error(e);
            throw new DAOException(e);
        }finally {
            try {
                if(preparedStatement!=null){
                    preparedStatement.close();
                }
                pool.closeConnection(connection);
            } catch (SQLException|ConnectionPoolException e) {
                log.error(e);
            }
        }
        return result;
    }

    public boolean changeVisits(User user){
        DBConnectionPool pool = DBConnectionPool.getInstance();
        Connection connection = null;
        boolean result = false;
        PreparedStatement preparedStatement= null;
        int visits = user.getVisits()  + 1;
        try {
            connection = pool.takeConnection();
            preparedStatement = connection.prepareStatement(SQL_UPDATE_USER_VISITS);
            preparedStatement.setInt(1,visits);
            preparedStatement.setInt(2, user.getId());
            if(preparedStatement.executeUpdate() >0)
                result = true;
        } catch (ConnectionPoolException|SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                if (preparedStatement!=null)
                    preparedStatement.close();
                pool.closeConnection(connection);
            } catch (ConnectionPoolException|SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }


}
