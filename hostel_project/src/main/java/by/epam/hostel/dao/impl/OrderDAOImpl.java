package by.epam.hostel.dao.impl;

import by.epam.hostel.dao.OrderDAO;
import by.epam.hostel.dao.pool.DBConnectionPool;
import by.epam.hostel.entity.Order;
import by.epam.hostel.entity.enumuration.OrderStatus;
import by.epam.hostel.entity.enumuration.PaymentStatus;
import by.epam.hostel.entity.enumuration.TypeOfDiscount;
import by.epam.hostel.exception.ConnectionPoolException;
import by.epam.hostel.exception.DAOException;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class OrderDAOImpl implements OrderDAO {
    Logger log = Logger.getLogger(OrderDAOImpl.class);
    private static final String ORDER_ID = "orders.idorder";
    private static final String ORDER_USER_ID = "orders.user_id";
    private static final String ORDER_DATE_OF_ARRIVAL = "orders.date_of_arrival";
    private static final String ORDER_DATE_OF_DEPARTURE = "orders.date_of_diparture";
    private static final String ORDER_DATE = "orders.orderDate";
    private static final String ORDER_PRICE = "orders.price";
    private static final String ORDER_STATUS = "orders.paymentState";
    private static final String ORDER_STATE = "orders.orderStatus";
    private static final String ORDER_PLACES = "places";
    private static final String ORDER_DISCOUNT = "orders.discount";
    private static final String SQL_FIND_ALL_ORDERS = "SELECT * FROM hostel_project.orders";
    private static final String SQL_FIND_ALL_ORDERS_BY_ID = "select orders.idorder, orders.user_id, orders.date_of_arrival," +
            " orders.date_of_diparture, orders.price, orders.orderDate,orders.orderStatus, orders.paymentState, " +
            "group_concat(order_detail.place_id separator ' ') as 'places' \n" +
            "from hostel_project.orders \n" +
            "left join hostel_project.order_detail on orders.idorder = order_detail.idorder\n" +
            "WHERE order_detail.idorder = ?\n" +
            "group by order_detail.idorder\n";
    private static final String SQL_FIND_ALL_ORDERS_BY_USER_ID = "SELECT orders.idorder, orders.user_id, " +
            "orders.date_of_arrival, orders.date_of_diparture, orders.price, orders.orderDate,orders.orderStatus," +
            " orders.paymentState, group_concat(order_detail.place_id SEPARATOR ' ') AS 'places' \n" +
            "FROM hostel_project.orders \n" +
            "LEFT JOIN hostel_project.order_detail ON orders.idorder = order_detail.idorder\n" +
            "WHERE user_id = ?\n" +
            "GROUP BY order_detail.idorder\n";
    private static final String SQL_CREATE_ORDER = "INSERT INTO hostel_project.orders(idorder,user_id,date_of_arrival, date_of_diparture,price," +
            " orderDate,paymentState,orderStatus) VALUES(?,?,?,?,?,?,?,?)";
    private static final String SQL_CHANGE_ORDER_STATUS = "UPDATE hostel_project.orders SET orderStatus=? WHERE idorder=?" ;
    private static final String SQL_UPDATE_ORDER_DISCOUNT = "UPDATE hostel_project.orders SET discount=? WHERE idorder = ?";
    private static final String SQL_CREATE_ORDER_DETAIL = "INSERT INTO hostel_project.order_detail(idorder,place_id) VALUES (?,?)";
    private static final String SQL_FIND_ORDERS = "select SQL_CALC_FOUND_ROWS orders.idorder, orders.user_id, orders.date_of_arrival," +
            " orders.date_of_diparture, orders.price, orders.orderDate,orders.orderStatus, orders.paymentState, " +
            "group_concat(order_detail.place_id separator ' ') as 'places' \n" +
            "from hostel_project.orders \n" +
            "left join hostel_project.order_detail on orders.idorder = order_detail.idorder\n" +
            "group by order_detail.idorder\n" +
            "LIMIT ?,?";
    private static final String SQL_DELETE_ORDER = "DELETE FROM hostel_project.orders WHERE orders.idorder=?";
    private static OrderDAOImpl instance = new OrderDAOImpl();
    public static OrderDAOImpl getInstance() {
        return instance;
    }
    private int noOfRecords;

    @Override
    public boolean delete(int id) throws DAOException {
        DBConnectionPool pool = DBConnectionPool.getInstance();

        boolean result = false;
        Connection connection = null;
        PreparedStatement prStatement = null;
        try {
            connection = pool.takeConnection();
            prStatement = connection.prepareStatement(SQL_DELETE_ORDER);
            prStatement.setInt(1, id);
            if (prStatement.executeUpdate() > 0) {
                result = true;
            }
        } catch (SQLException | ConnectionPoolException e) {
            throw new DAOException(e);
        } finally {
            try {
                if (prStatement != null) {
                    prStatement.close();
                }
                pool.closeConnection(connection);
            } catch (SQLException | ConnectionPoolException e) {
                throw new DAOException(e);
            }
        }
        return result;
    }

    @Override
    public int getNoOfRecords() {
        return noOfRecords;
    }
    private void createOrderDetail(Order order){
        log.info("createOrderDetail");
        DBConnectionPool pool = DBConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        boolean result = false;
        try {
            connection = pool.takeConnection();
            preparedStatement = connection.prepareStatement(SQL_CREATE_ORDER_DETAIL);
            for (int i : order.getPlaceID()){
                preparedStatement.setInt(1,order.getId());
                preparedStatement.setInt(2,i);
                if(preparedStatement.executeUpdate() > 0)
                    result = true;
            }
        } catch (ConnectionPoolException| SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                    if (preparedStatement != null)
                        preparedStatement.close();
                    pool.closeConnection(connection);
                } catch (ConnectionPoolException|SQLException e) {
                    e.printStackTrace();
                }
        }
    }
    @Override
    public List<Order> findAll() {
        log.info("OrderDAOImpl.findAll()");
        DBConnectionPool pool = DBConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ArrayList<Order> orders = new ArrayList<>();
        try {
            connection = pool.takeConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_ALL_ORDERS);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Order order = new Order();
                order.setId(resultSet.getInt(ORDER_ID));
                order.setUsersID(resultSet.getInt(ORDER_USER_ID));
                order.setArrival(resultSet.getDate(ORDER_DATE_OF_ARRIVAL));
                order.setDeparture(resultSet.getDate(ORDER_DATE_OF_DEPARTURE));
                order.setPaymentStatus(PaymentStatus.valueOf(resultSet.getString(ORDER_STATUS)));
                order.setOrderStatus(OrderStatus.valueOf(resultSet.getString(ORDER_STATE)));
                orders.add(order);
            }
        } catch (ConnectionPoolException|SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null)
                    preparedStatement.close();
                pool.closeConnection(connection);
            } catch (SQLException|ConnectionPoolException e) {
                e.printStackTrace();
            }
        }
        return orders;
    }

    public boolean updateDiscount(int id, TypeOfDiscount typeOfDiscount) throws DAOException {
        log.info("updateDiscount");
        DBConnectionPool pool = DBConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        boolean result = false;
        try {
            connection = pool.takeConnection();
            preparedStatement = connection.prepareStatement(SQL_UPDATE_ORDER_DISCOUNT);
            preparedStatement.setInt(1,DiscountDAOImpl.getInstance().findDiscountByType(typeOfDiscount).getId());
            preparedStatement.setInt(2,id);
            if(preparedStatement.executeUpdate() > 0)
                result = true;
        } catch (ConnectionPoolException|SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                if (preparedStatement != null)
                    preparedStatement.close();
                pool.closeConnection(connection);

            } catch (ConnectionPoolException|SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
    @Override
    public Order findEntityById(Integer id){
        log.info("findEntityById");
        DBConnectionPool pool = DBConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        Order order = new Order();
        try {
            connection = pool.takeConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_ALL_ORDERS_BY_ID);
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                order.setId(resultSet.getInt(ORDER_ID));
                order.setUsersID(resultSet.getInt(ORDER_USER_ID));
                order.setArrival(resultSet.getDate(ORDER_DATE_OF_ARRIVAL));
                //order.setOrderDate(resultSet.getDate(ORDER_DATE));
                order.setDeparture(resultSet.getDate(ORDER_DATE_OF_DEPARTURE));
                order.setPaymentStatus(PaymentStatus.valueOf(resultSet.getString(ORDER_STATUS)));
                order.setOrderStatus(OrderStatus.valueOf(resultSet.getString(ORDER_STATE)));
                order.setPrice(resultSet.getFloat(ORDER_PRICE));
                int[] numArr = Arrays.stream(resultSet.getString(ORDER_PLACES).split(" ")).mapToInt(Integer::parseInt).toArray();
                for(int i = 0; i < numArr.length; i++)
                    order.getPlaceID().add(numArr[i]);
            }
        } catch (ConnectionPoolException|SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                if (preparedStatement != null)
                    preparedStatement.close();
                pool.closeConnection(connection);
            } catch (ConnectionPoolException|SQLException e) {
                e.printStackTrace();
            }
        }
        return order;
    }

    @Override
    public List<Order> findOrderByUserID(int id) {
        log.info("findOrderByUserID");
        DBConnectionPool pool = DBConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ArrayList<Order> orders = new ArrayList<>();
        Order order;
        try {
            connection = pool.takeConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_ALL_ORDERS_BY_USER_ID);
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                order = new Order();
                order.setId(resultSet.getInt(ORDER_ID));
                order.setUsersID(resultSet.getInt(ORDER_USER_ID));
                order.setArrival(resultSet.getDate(ORDER_DATE_OF_ARRIVAL));
                order.setDeparture(resultSet.getDate(ORDER_DATE_OF_DEPARTURE));
                order.setPaymentStatus(PaymentStatus.valueOf(resultSet.getString(ORDER_STATUS)));
                order.setOrderStatus(OrderStatus.valueOf(resultSet.getString(ORDER_STATE)));
                order.setPrice(resultSet.getFloat(ORDER_PRICE));
                int[] numArr = Arrays.stream(resultSet.getString(ORDER_PLACES).split(" ")).mapToInt(Integer::parseInt).toArray();
                for(int i = 0; i < numArr.length; i++)
                    order.getPlaceID().add(numArr[i]);
//                order.getPlaceID() = Arrays.asList(numArr);
                orders.add(order);
            }
        } catch (ConnectionPoolException|SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null)
                    preparedStatement.close();
                pool.closeConnection(connection);
            } catch (ConnectionPoolException|SQLException e) {
                e.printStackTrace();
            }
        }
        return orders;
    }

    @Override
    public boolean changeOrderStatus(int id,OrderStatus paymentStatus) {
        log.info("changeOrderStatus");
        DBConnectionPool pool = DBConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        boolean result = false;
        try {
            connection = pool.takeConnection();
            preparedStatement = connection.prepareStatement(SQL_CHANGE_ORDER_STATUS);
            preparedStatement.setString(1, paymentStatus.toString());
            preparedStatement.setInt(2,id);
            if (preparedStatement.executeUpdate() > 0)
                result = true;
        } catch (ConnectionPoolException|SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                if(preparedStatement != null)
                    preparedStatement.close();
                pool.closeConnection(connection);
            } catch (ConnectionPoolException|SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @Override
    public boolean createOrder(Order order) {
        log.info("createOrder");
        DBConnectionPool pool = DBConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        boolean result = false;
        try {
            connection = pool.takeConnection();
            preparedStatement = connection.prepareStatement(SQL_CREATE_ORDER);
            preparedStatement.setInt(1,order.getId());
            if ( order.getUsersID()!= 0) {
                preparedStatement.setInt(2, order.getUsersID());
                preparedStatement.setString(8,OrderStatus.processing.toString());
            }
            else {
                preparedStatement.setString(2, null);
                preparedStatement.setString(8,OrderStatus.confirmed.toString());
            }
            preparedStatement.setDate(3, new java.sql.Date(order.getArrival().getTime()));
            preparedStatement.setDate(4, new java.sql.Date(order.getDeparture().getTime()));
            preparedStatement.setFloat(5,order.getPrice());
            preparedStatement.setDate(6,new java.sql.Date(order.getOrderDate().getTime()));
            preparedStatement.setString(7,order.getPaymentStatus().toString());
            if (preparedStatement.executeUpdate() >0) {
                result = true;
                createOrderDetail(order);
            }
        } catch (ConnectionPoolException|SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                if (preparedStatement != null)
                    preparedStatement.close();
                pool.closeConnection(connection);
            } catch (SQLException|ConnectionPoolException e) {
                e.printStackTrace();
            }
        }
        return result;

    }

    public List<Order> pageViewAll(int offset, int noOfRecords){
        log.info("pageViewAll");
        DBConnectionPool pool = DBConnectionPool.getInstance();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        List<Order> orders = new ArrayList<>();
        Order order;
        try {
            connection = pool.takeConnection();
            preparedStatement = connection.prepareStatement(SQL_FIND_ORDERS);
            preparedStatement.setInt(1,offset);
            preparedStatement.setInt(2,noOfRecords);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                order = new Order();
                order.setId(resultSet.getInt(ORDER_ID));
                order.setUsersID(resultSet.getInt(ORDER_USER_ID));
                order.setArrival(resultSet.getDate(ORDER_DATE_OF_ARRIVAL));
                order.setDeparture(resultSet.getDate(ORDER_DATE_OF_DEPARTURE));
                order.setPaymentStatus(PaymentStatus.valueOf(resultSet.getString(ORDER_STATUS)));
                order.setOrderStatus(OrderStatus.valueOf(resultSet.getString(ORDER_STATE)));
                order.setPrice(resultSet.getFloat(ORDER_PRICE));
                int[] numArr = Arrays.stream(resultSet.getString(ORDER_PLACES).split(" ")).mapToInt(Integer::parseInt).toArray();
                for(int i = 0; i < numArr.length; i++)
                    order.getPlaceID().add(numArr[i]);

                orders.add(order);
            }
            resultSet.close();
            resultSet = preparedStatement.executeQuery("SELECT FOUND_ROWS()");
            if (resultSet.next())
                this.noOfRecords = resultSet.getInt(1);
        } catch (ConnectionPoolException|SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null)
                    preparedStatement.close();
                pool.closeConnection(connection);
            } catch (SQLException|ConnectionPoolException e) {
                e.printStackTrace();
            }
        }
        return orders;

    }
}
