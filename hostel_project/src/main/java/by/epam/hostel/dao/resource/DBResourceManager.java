package by.epam.hostel.dao.resource;

import java.util.ResourceBundle;

/**
 * Created by NotePad.by on 21.08.2016.
 */
public class DBResourceManager {
    private final static String PROPERTY_FILE="db";
    private final static DBResourceManager instance = new DBResourceManager();
    private static ResourceBundle bundle = ResourceBundle.getBundle(PROPERTY_FILE);

    public static DBResourceManager getInstance() {
        return instance;
    }
    public static String getValue(String key){
        return bundle.getString(key);
    }

}
