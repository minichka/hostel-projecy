package by.epam.hostel.dao;

import by.epam.hostel.entity.Entity;
import by.epam.hostel.exception.DAOException;

import java.util.List;

/**
 * Created by NotePad.by on 16.08.2016.
 */
public interface GenericDAO<K, T extends Entity> {
    List<T> findAll() throws DAOException;
    T findEntityById(K id) throws DAOException;

}
