package by.epam.hostel.dao.impl;

import by.epam.hostel.dao.PlaceDAO;
import by.epam.hostel.dao.pool.DBConnectionPool;
import by.epam.hostel.entity.Place;
import by.epam.hostel.entity.enumuration.StateOfRoom;
import by.epam.hostel.entity.enumuration.TypeOfRoom;
import by.epam.hostel.exception.ConnectionPoolException;
import by.epam.hostel.exception.DAOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * Created by NotePad.by on 27.08.2016.
 */
public class PlaceDAOImpl implements PlaceDAO {

    private final static Logger log = Logger.getLogger(PlaceDAOImpl.class);

    public static PlaceDAOImpl getInstance() {
        log.info("PlaceDAO instance");
        return instance;
    }

    private static PlaceDAOImpl instance = new PlaceDAOImpl();

    private static final String PLACE_ID = "places.idPlaces";
    private static final String PLACE_TYPE = "places.Type_of_room";
    private static final String PLACE_PRICE = "places.Price";
    private static final String PLACE_STATE = "places.isFree";

    private static final String SQL_SELECT_ALL_PLACES="SELECT * FROM hostel_project.places";
    private static final String SQL_SELECT_BY_TYPE="SELECT * FROM hostel_project.places WHERE Type_of_room=?";
    private static final String SQL_UPDATE_STATUS="UPDATE hostel_project.places SET isFree=? WHERE places.idPlaces=?";
    private static final String SQL_SELECT_BY_ID="SELECT * FROM hostel_project.places WHERE idPlaces=?";
    private static final String SQL_SELECT_BY_STATUS_AND_TYPE = "SELECT * FROM hostel_project.places WHERE Type_of_room=? AND isFree=?";

    public List<Place> findPlaceByType(TypeOfRoom type) throws DAOException {
        log.info("findPlaceByType()");
        DBConnectionPool pool = DBConnectionPool.getInstance();

        ArrayList<Place> places = new ArrayList<>();

        Connection connection= null;
        PreparedStatement preparedStatement = null;

        try{
            connection = pool.takeConnection();
            preparedStatement = connection.prepareStatement(SQL_SELECT_BY_STATUS_AND_TYPE);
            preparedStatement.setString(1, type.getType());
            preparedStatement.setBoolean(2, true);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Place place = new Place();
                place.setId(resultSet.getInt(PLACE_ID));
                place.setTypeOfRoom(TypeOfRoom.valueOf(resultSet.getString(PLACE_TYPE)));
                place.setPrice(resultSet.getFloat(PLACE_PRICE));
                place.setStateOfRoom(resultSet.getBoolean(PLACE_STATE));
                places.add(place);
            }
        } catch (ConnectionPoolException|SQLException e) {
            log.error(e);
            throw new DAOException(e);
        } finally {
            try {
                if(preparedStatement!=null){
                    preparedStatement.close();
                }
                pool.closeConnection(connection);
            } catch (SQLException|ConnectionPoolException e) {
                log.error(e);
            }
        }
        return places;
    }

    @Override
    public List<Place> findPlaceByStatus(boolean isFree) throws DAOException {
        DBConnectionPool pool = DBConnectionPool.getInstance();

        ArrayList<Place> places = new ArrayList<>();

        Connection connection= null;
        PreparedStatement preparedStatement = null;

        try{
            connection = pool.takeConnection();
            preparedStatement = connection.prepareStatement(SQL_SELECT_BY_TYPE);
            preparedStatement.setBoolean(1, isFree);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Place place = new Place();
                place.setId(resultSet.getInt(PLACE_ID));
                place.setTypeOfRoom(TypeOfRoom.valueOf(resultSet.getString(PLACE_TYPE)));
                place.setPrice(resultSet.getFloat(PLACE_PRICE));
                place.setStateOfRoom(resultSet.getBoolean(PLACE_STATE));
                places.add(place);
            }
        } catch (ConnectionPoolException|SQLException e) {
            log.error(e);
            throw new DAOException(e);
        } finally {
            try {
                if(preparedStatement!=null){
                    preparedStatement.close();
                }
                pool.closeConnection(connection);
            } catch (SQLException|ConnectionPoolException e) {
                log.error(e);
            }
        }
        return places;
    }

    @Override
    public boolean changePlaceStatus(int id, boolean isFree) throws DAOException {
        DBConnectionPool pool = DBConnectionPool.getInstance();
        boolean result = false;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try{
            connection = pool.takeConnection();
            preparedStatement = connection.prepareStatement(SQL_UPDATE_STATUS);
            preparedStatement.setBoolean(1,isFree);
            preparedStatement.setInt(2,id);
            if (preparedStatement.executeUpdate() == 1)
                result = true;
        } catch (ConnectionPoolException|SQLException e) {
            log.error(e);
            throw new DAOException(e);
        } finally {
            try {
                if(preparedStatement!=null){
                    preparedStatement.close();
                }
                pool.closeConnection(connection);
            } catch (SQLException|ConnectionPoolException e) {
                log.error(e);
            }
        }
        return result;
    }

    @Override
    public Place findEntityById(Integer id) throws DAOException {
        DBConnectionPool pool = DBConnectionPool.getInstance();

        Place place = null;

        Connection connection= null;
        PreparedStatement preparedStatement = null;

        try{
            connection = pool.takeConnection();
            preparedStatement = connection.prepareStatement(SQL_SELECT_BY_ID);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                place = new Place();
                place.setId(resultSet.getInt(PLACE_ID));
                place.setTypeOfRoom(TypeOfRoom.valueOf(resultSet.getString(PLACE_TYPE)));
                place.setPrice(resultSet.getFloat(PLACE_PRICE));
                place.setStateOfRoom(resultSet.getBoolean(PLACE_STATE));
            }
        } catch (ConnectionPoolException|SQLException e) {
            log.error(e);
            throw new DAOException(e);
        } finally {
            try {
                if(preparedStatement!=null){
                    preparedStatement.close();
                }
                pool.closeConnection(connection);
            } catch (SQLException|ConnectionPoolException e) {
                log.error(e);
            }
        }
        return place;
    }

    @Override
    public List<Place> findAll() throws DAOException {
        DBConnectionPool pool = DBConnectionPool.getInstance();

        ArrayList<Place> places = new ArrayList<>();

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = pool.takeConnection();
            preparedStatement = connection.prepareStatement(SQL_SELECT_ALL_PLACES);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                Place place = new Place();
                place.setId(resultSet.getInt(PLACE_ID));
                place.setTypeOfRoom(TypeOfRoom.valueOf(resultSet.getString(PLACE_TYPE)));
                place.setPrice(resultSet.getFloat(PLACE_PRICE));
                place.setStateOfRoom(resultSet.getBoolean(PLACE_STATE));
                places.add(place);
            }
        } catch (ConnectionPoolException|SQLException e) {
            log.error(e);
            throw new DAOException(e);
        } finally {
            try {
                if(preparedStatement!=null){
                    preparedStatement.close();
                }
                pool.closeConnection(connection);
            } catch (SQLException|ConnectionPoolException e) {
                log.error(e);
            }
        }

        return places;
    }

    public int countFreeRooms(TypeOfRoom typeOfRoom, boolean isFree) throws DAOException {
        log.info("countFreeRooms");

        DBConnectionPool pool = DBConnectionPool.getInstance();

        //ArrayList<Place> places = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int amount = 0;
        try {
            connection = pool.takeConnection();
            preparedStatement = connection.prepareStatement(SQL_SELECT_BY_STATUS_AND_TYPE);
            preparedStatement.setString(1, typeOfRoom.getType());
            preparedStatement.setBoolean(2, isFree);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                amount++;
            }
        } catch (SQLException | ConnectionPoolException e) {
            log.error(e);
            throw new DAOException(e);
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                pool.closeConnection(connection);
            } catch (SQLException | ConnectionPoolException e) {
                log.error(e);
            }


        }
        return amount;
    }
}
