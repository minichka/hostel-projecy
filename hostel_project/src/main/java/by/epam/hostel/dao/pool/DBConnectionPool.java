package by.epam.hostel.dao.pool;

import by.epam.hostel.dao.resource.DBParameter;
import by.epam.hostel.dao.resource.DBResourceManager;
import by.epam.hostel.exception.ConnectionPoolException;

import java.sql.*;


import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.log4j.Logger;
/**
 * Created by NotePad.by on 21.08.2016.
 */
public class DBConnectionPool {
    private static final Logger log = Logger.getLogger(String.valueOf(DBConnectionPool.class));
    private static final String DRIVER = DBResourceManager.getValue(DBParameter.DB_DRIVER);
    private static final String URL = DBResourceManager.getValue(DBParameter.DB_URL);
    private static final String USER = DBResourceManager.getValue(DBParameter.DB_USER);
    private static final String PASSWORD = DBResourceManager.getValue(DBParameter.DB_PASSWORD);
    private static final int SIZE = Integer.parseInt(DBResourceManager.getValue(DBParameter.DB_POOL_SIZE));
    private static final String ERROR_INFO = "Connection not in the usedConnections";
    private BlockingQueue<ConnectionWrapper> availableConnections;
    private BlockingQueue<ConnectionWrapper> usedConnections;
    private static DBConnectionPool instance;
    private static final Lock lock = new ReentrantLock();
    private String url;
    private String user;
    private String password;

    private Connection getConnection() throws ConnectionPoolException{
        log.info("DBConnectionPool.getConnection()");
        Connection connection;
        try {
            connection = DriverManager.getConnection(url,user,password);
        } catch (SQLException e) {
            throw new ConnectionPoolException(e);
        }
        return connection;

    }
    public void initializeAvailableConnection() throws ConnectionPoolException{

        log.info("initializeAvailableConnection()");
        log.info("av.con. before init:" + availableConnections.size());
        log.info("used.con. before init:" + usedConnections.size());

        for (int i = 0; i < SIZE; i++){
            Connection connection = getConnection();
            ConnectionWrapper connectionWrapper = new ConnectionWrapper(connection);
            availableConnections.add(connectionWrapper);
        }

        log.info("av.con. after init:" + availableConnections.size());
        log.info("used.con. after init:" + usedConnections.size());


    }

    private DBConnectionPool(String url, String user, String password) {
        log.info("DBConnectionPool()");
        try {
            Class.forName(DRIVER);
            this.url = url;
            this.user = user;
            this.password = password;
            availableConnections = new ArrayBlockingQueue<ConnectionWrapper>(SIZE);
            usedConnections = new ArrayBlockingQueue<ConnectionWrapper>(SIZE);
        } catch (ClassNotFoundException e) {
            //&&&&7
        }
    }

    public static DBConnectionPool getInstance(){
        log.info("DBConnectionPool getInstance()");
        try {
            lock.lock();
            if (instance == null) {
                log.info("Instance null");
                instance = new DBConnectionPool(URL, USER, PASSWORD);
                //instance.initializeAvailableConnection();
            }

        } finally {
            lock.unlock();
        }
        return instance;
    }

    public Connection takeConnection() throws ConnectionPoolException{
        log.info("DBConnectionPool.takeConnection()");
        ConnectionWrapper newConnection;
        try{
            log.info("try to take connection");
            newConnection = availableConnections.take();
            log.info("new connection is taken");
            usedConnections.put(newConnection);
        } catch (InterruptedException e) {
            throw new ConnectionPoolException();
        }
        log.info("av.con. after take:" + availableConnections.size());
        log.info("used.con. after take:" + usedConnections.size());
        return newConnection;
    }

    public void closeConnection(Connection c) throws ConnectionPoolException {
        log.info("DBConnectionPool.closeConnection()");
        log.info("Connection to close:" + c);
        log.info("Is contain:" + usedConnections.contains(c));

        try{
            if (c!=null){
                if (usedConnections.remove(c)){
                    availableConnections.put((ConnectionWrapper) c);
                }
            }
        } catch (InterruptedException e) {
            throw new ConnectionPoolException();
        }

    }
    public  void destroyConnectionPool() throws ConnectionPoolException {
        log.info("DBConnectionPool.destroyConnectionPool()");
        log.info("av.con. before destroy:" + availableConnections.size());
        log.info("used.con. before destroy:" + usedConnections.size());
        try {
            closeConnectionQueue(availableConnections);
        } catch (ConnectionPoolException e) {
            throw new ConnectionPoolException("Can't correctly destroy connection pool.");
        }finally {
            closeConnectionQueue(usedConnections);
        }
        log.info("av.con. after destroy:" + availableConnections.size());
        log.info("used.con. after destroy:" + usedConnections.size());


    }

    private void closeConnectionQueue(BlockingQueue<ConnectionWrapper> queue) throws ConnectionPoolException {
        log.info("DBConnectionPool.closeConnectionQueue()");
        boolean errorStatus = false;
        for(Connection connection : queue){
            try {
                if (connection != null) {
                    ((ConnectionWrapper) connection).dispose();
                }
            }catch(SQLException e){
                errorStatus = true;
            }
        }
        if (errorStatus)
            throw  new ConnectionPoolException("Can't close connection queue.");

    }


    private class ConnectionWrapper implements Connection{
        private Connection connection;

        public ConnectionWrapper(Connection connection) {
            this.connection = connection;
        }

        public Statement createStatement() throws SQLException {
            return connection.createStatement();
        }

        public PreparedStatement prepareStatement(String sql) throws SQLException {
            return connection.prepareStatement(sql);
        }

        public CallableStatement prepareCall(String sql) throws SQLException {
            return connection.prepareCall(sql);
        }

        public String nativeSQL(String sql) throws SQLException {
            return connection.nativeSQL(sql);
        }

        public void setAutoCommit(boolean autoCommit) throws SQLException {
            connection.setAutoCommit(autoCommit);
        }

        public boolean getAutoCommit() throws SQLException {
            return connection.getAutoCommit();
        }

        public void commit() throws SQLException {
            connection.commit();
        }

        public void rollback() throws SQLException {
            connection.rollback();
        }

        public void close() throws SQLException {
            log.info("WrapperConnection.close()");
            log.info("Connection in close():" + connection);

            if (connection.isClosed()) {
                throw new SQLException("Can't close closed exception.");
            }
            try {
                closeConnection(connection);
            } catch (ConnectionPoolException e) {
                log.error(e);
            }
        }

        private void dispose() throws SQLException {
            log.info("WrapperConnection.dispose()");
            this.connection.close();
        }
        public boolean isClosed() throws SQLException {
            return connection.isClosed();
        }

        public DatabaseMetaData getMetaData() throws SQLException {
            return connection.getMetaData();
        }

        public void setReadOnly(boolean readOnly) throws SQLException {
            connection.setReadOnly(readOnly);
        }

        public boolean isReadOnly() throws SQLException {
            return connection.isReadOnly();
        }

        public void setCatalog(String catalog) throws SQLException {
            connection.setCatalog(catalog);
        }

        public String getCatalog() throws SQLException {
            return connection.getCatalog();
        }

        public void setTransactionIsolation(int level) throws SQLException {
            connection.setTransactionIsolation(level);
        }

        public int getTransactionIsolation() throws SQLException {
            return connection.getTransactionIsolation();
        }

        public SQLWarning getWarnings() throws SQLException {
            return connection.getWarnings();
        }

        public void clearWarnings() throws SQLException {
            connection.clearWarnings();
        }

        public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
            return connection.createStatement(resultSetType, resultSetConcurrency);
        }

        public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
            return connection.prepareStatement(sql,resultSetType,resultSetConcurrency);
        }

        public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
            return connection.prepareCall(sql, resultSetType, resultSetConcurrency);
        }
        public Map<String, Class<?>> getTypeMap() throws SQLException {
            return connection.getTypeMap();
        }

        public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
            connection.setTypeMap(map);
        }

        public void setHoldability(int holdability) throws SQLException {
            connection.setHoldability(holdability);
        }

        public int getHoldability() throws SQLException {
            return connection.getHoldability();
        }

        public Savepoint setSavepoint() throws SQLException {
            return connection.setSavepoint();
        }

        public Savepoint setSavepoint(String name) throws SQLException {
            return connection.setSavepoint(name);
        }

        public void rollback(Savepoint savepoint) throws SQLException {
            connection.rollback(savepoint);
        }

        public void releaseSavepoint(Savepoint savepoint) throws SQLException {
            connection.releaseSavepoint(savepoint);
        }

        public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
            return connection.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
        }

        public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
            return connection.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
        }

        public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
            return connection.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
        }

        public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
            return connection.prepareStatement(sql, autoGeneratedKeys);
        }

        public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
            return connection.prepareStatement(sql, columnIndexes);
        }

        public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
            return connection.prepareStatement(sql, columnNames);
        }

        public Clob createClob() throws SQLException {
            return connection.createClob();
        }

        public Blob createBlob() throws SQLException {
            return connection.createBlob();
        }

        public NClob createNClob() throws SQLException {
            return connection.createNClob();
        }

        public SQLXML createSQLXML() throws SQLException {
            return connection.createSQLXML();
        }

        public boolean isValid(int timeout) throws SQLException {
            return connection.isValid(timeout);
        }

        public void setClientInfo(String name, String value) throws SQLClientInfoException {
            connection.setClientInfo(name,value);
        }

        public void setClientInfo(Properties properties) throws SQLClientInfoException {
            connection.setClientInfo(properties);
        }

        public String getClientInfo(String name) throws SQLException {
            return connection.getClientInfo(name);
        }

        public Properties getClientInfo() throws SQLException {
            return connection.getClientInfo();
        }

        public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
            return connection.createArrayOf(typeName,elements);
        }

        public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
            return connection.createStruct(typeName,attributes);
        }

        public void setSchema(String schema) throws SQLException {
            connection.setSchema(schema);
        }

        public String getSchema() throws SQLException {
            return connection.getSchema();
        }

        public void abort(Executor executor) throws SQLException {
            connection.abort(executor);
        }

        public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
            connection.setNetworkTimeout(executor,milliseconds);
        }

        public int getNetworkTimeout() throws SQLException {
            return connection.getNetworkTimeout();
        }

        public <T> T unwrap(Class<T> iface) throws SQLException {
            return connection.unwrap(iface);
        }

        public boolean isWrapperFor(Class<?> iface) throws SQLException {
            return connection.isWrapperFor(iface);
        }
    }





}
