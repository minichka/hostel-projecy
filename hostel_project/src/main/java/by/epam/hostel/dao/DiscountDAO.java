package by.epam.hostel.dao;

import by.epam.hostel.entity.Discount;
import by.epam.hostel.entity.enumuration.TypeOfDiscount;
import by.epam.hostel.exception.DAOException;

import java.util.List;

/**
 * Created by NotePad.by on 16.08.2016.
 */
public interface DiscountDAO extends GenericDAO<Integer, Discount> {
    @Override
    List<Discount> findAll() throws DAOException;

    @Override
    Discount findEntityById(Integer id) throws DAOException ;

    Discount findDiscountByType(TypeOfDiscount type) throws DAOException;

}

