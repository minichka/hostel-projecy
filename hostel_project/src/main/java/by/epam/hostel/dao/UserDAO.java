package by.epam.hostel.dao;

import by.epam.hostel.entity.User;
import by.epam.hostel.entity.enumuration.TypeOfDiscount;
import by.epam.hostel.exception.DAOException;

import java.util.List;

/**
 * Created by NotePad.by on 16.08.2016.
 */
public interface UserDAO extends GenericDAO<Integer, User> {

    boolean changeVisits(User user);

    @Override
    User findEntityById(Integer id) throws DAOException;

    boolean updateDiscount(int id, TypeOfDiscount typeOfDiscount) throws DAOException;

    List<User> findUserByStatus(boolean status) throws DAOException;
    List<User> findUserByRole(boolean isAdmin) throws DAOException;
    boolean changeStatus(String email, boolean status) throws DAOException;
    boolean isExist(String email) throws DAOException;
    boolean signUp(User user) throws DAOException;
    User findUserByEmail(String email) throws DAOException;
    boolean delete(int id) throws DAOException;





}
