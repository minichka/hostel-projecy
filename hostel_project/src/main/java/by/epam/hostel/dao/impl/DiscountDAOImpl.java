package by.epam.hostel.dao.impl;

import by.epam.hostel.dao.DiscountDAO;
import by.epam.hostel.dao.pool.DBConnectionPool;
import by.epam.hostel.entity.Discount;
import by.epam.hostel.entity.enumuration.TypeOfDiscount;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import by.epam.hostel.exception.ConnectionPoolException;
import by.epam.hostel.exception.DAOException;
import org.apache.log4j.Logger;


public class DiscountDAOImpl implements DiscountDAO {

    private static final Logger log = Logger.getLogger(DiscountDAOImpl.class);

    private static DiscountDAOImpl instance = new DiscountDAOImpl();

    private static final String DISCOUNT_ID = "discount.iddiscount";
    private static final String DISCOUNT_TYPE = "discount.type_of_discount";
    private static final String DISCOUNT_VALUE = "discount.discount";

    private static final String SQL_SELECT_ALL_DISCOUNTS = "SELECT * FROM hostel_project.discount";
    private static final String SQL_SELECT_DISCOUNT_BY_TYPE = "SELECT * FROM hostel_project.discount WHERE discount.type_of_discount=?";
    private static final String SQL_SELECT_DISCOUNT_BY_ID = "SELECT * FROM hostel_project.discount WHERE discount.iddiscount=?";
    private static final String SQL_SELECT_DISCOUNT_BY_VALUE ="SELECT * FROM hostel_project.discount WHERE discount.discount=?";
    public DiscountDAOImpl() {
        super();
    }

    public static DiscountDAOImpl getInstance() {
        log.info("DiscountDAOImpl.getInstance()");
        return instance;
    }

    @Override
    public List<Discount> findAll() throws DAOException {
        log.info("DiscountDAOImpl.findAll()");

        DBConnectionPool pool = DBConnectionPool.getInstance();
        ArrayList<Discount> discounts = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try{
            connection = pool.takeConnection();
            preparedStatement = connection.prepareStatement(SQL_SELECT_ALL_DISCOUNTS);
            ResultSet resultSet = preparedStatement.executeQuery();
            //Map map =connection.getTypeMap();

            while(resultSet.next()){
                Discount discount = new Discount();
                discount.setId(resultSet.getInt(DISCOUNT_ID));
                discount.setTypeOfDiscount(TypeOfDiscount.valueOf(resultSet.getString(DISCOUNT_TYPE)));
                discount.setDiscount(resultSet.getFloat(DISCOUNT_VALUE));
                discounts.add(discount);
            }

        } catch (SQLException |ConnectionPoolException e ) {
            log.info(e);
            throw new DAOException(e);
        }
        finally {
            try{
                if(preparedStatement!=null)
                    preparedStatement.close();
                pool.closeConnection(connection);
            } catch (SQLException |ConnectionPoolException e) {
                log.error(e);
            }
        }

        return discounts;
    }

    @Override
    public Discount findEntityById(Integer id) throws DAOException {
        log.info("DiscountDAOImpl.findEntityById");
        DBConnectionPool pool = DBConnectionPool.getInstance();

        Discount discount =null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try{
           connection = pool.takeConnection();
            preparedStatement = connection.prepareStatement(SQL_SELECT_DISCOUNT_BY_ID);
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                discount = new Discount();
                discount.setId(resultSet.getInt(DISCOUNT_ID));
                discount.setTypeOfDiscount(TypeOfDiscount.valueOf(resultSet.getString(DISCOUNT_TYPE)));
                discount.setDiscount(resultSet.getFloat(DISCOUNT_VALUE));

            }
        } catch (ConnectionPoolException|SQLException e) {
            log.error(e);
            throw new DAOException(e);
        }finally {
            try{
                if(preparedStatement != null)
                    preparedStatement.close();
                pool.closeConnection(connection);

            } catch (SQLException|ConnectionPoolException e) {
                log.error(e);
            }
        }
        return discount;
    }

    @Override
    public Discount findDiscountByType(TypeOfDiscount type) throws DAOException {
        log.info("DiscountDAOImpl.findDiscountByType");
        DBConnectionPool pool = DBConnectionPool.getInstance();

        Discount discount = null;
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = pool.takeConnection();
            preparedStatement = connection.prepareStatement(SQL_SELECT_DISCOUNT_BY_TYPE);
            preparedStatement.setString(1, type.getType());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                discount = new Discount();
                discount.setId(resultSet.getInt(DISCOUNT_ID));
                discount.setTypeOfDiscount(TypeOfDiscount.valueOf(resultSet.getString(DISCOUNT_TYPE)));
                discount.setDiscount(resultSet.getFloat(DISCOUNT_VALUE));
            }
        } catch (ConnectionPoolException|SQLException e) {
            log.error(e);
            throw new DAOException(e);
        } finally {
            try{
                if(preparedStatement != null){
                    preparedStatement.close();
                }
                pool.closeConnection(connection);
            } catch (SQLException|ConnectionPoolException e) {
                log.error(e);
            }
        }
        return discount;
    }
}
