package by.epam.hostel.dao;

import by.epam.hostel.entity.Place;
import by.epam.hostel.entity.enumuration.StateOfRoom;
import by.epam.hostel.entity.enumuration.TypeOfRoom;
import by.epam.hostel.exception.DAOException;

import java.util.List;

/**
 * Created by NotePad.by on 16.08.2016.
 */
public interface PlaceDAO extends GenericDAO<Integer,Place> {

    List<Place> findPlaceByType(TypeOfRoom type) throws DAOException;
    List<Place> findPlaceByStatus(boolean isFree) throws DAOException;
    boolean changePlaceStatus(int id, boolean isFree) throws DAOException;
    public int countFreeRooms(TypeOfRoom typeOfRoom, boolean isFree) throws DAOException;
    @Override
    Place findEntityById(Integer id) throws DAOException;
    @Override
    List<Place> findAll() throws DAOException;
}
