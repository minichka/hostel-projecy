package by.epam.hostel.dao;

import by.epam.hostel.entity.Order;
import by.epam.hostel.entity.enumuration.OrderStatus;
import by.epam.hostel.entity.enumuration.PaymentStatus;
import by.epam.hostel.entity.enumuration.TypeOfDiscount;
import by.epam.hostel.exception.DAOException;

import java.util.List;

/**
 * Created by NotePad.by on 16.08.2016.
 */
public interface OrderDAO extends GenericDAO<Integer,Order> {
    @Override
    List<Order> findAll();

    @Override
    Order findEntityById(Integer id);

    List<Order> findOrderByUserID(int id);
    boolean changeOrderStatus(int id, OrderStatus paymentStatus);
    boolean createOrder(Order order);
    boolean updateDiscount(int id, TypeOfDiscount typeOfDiscount) throws DAOException;
    List<Order> pageViewAll(int offset, int noOfRecords);
    boolean delete(int id) throws DAOException;
    int getNoOfRecords();
}
