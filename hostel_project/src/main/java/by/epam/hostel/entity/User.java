package by.epam.hostel.entity;

import by.epam.hostel.entity.enumuration.TypeOfDiscount;

import java.util.Date;

/**
 * Created by NotePad.by on 15.08.2016.
 */
public class User extends Entity {
    //private static int amountOfUsers = 0;
    private int id;
    private String email;
    private String password;
    private String name;
    private String lastName;
    private boolean isActive;
    private int visits;
    private float discount;
    private Date birthday;
    private boolean isAdmin;

    public User() {
    }

    public User(int id, String email, String password, Date birthday, boolean isActive, boolean isAdmin) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.isActive = isActive;
        this.visits = 0;
        this.birthday = birthday;
        this.discount = new Discount(TypeOfDiscount.NEW_USER).getId();
        this.isAdmin = isAdmin;
    }

    public User(String email, String password, boolean isActive, boolean isAdmin, String name, String lastName) {
        //this.birthday = birthday;
        this.email = email;
        this.password = password;
        this.isActive = isActive;
        this.isAdmin = isAdmin;
        this.visits = 0;
        this.name = name;
        this.lastName = lastName;
        this.discount = 1;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getVisits() {
        return visits;
    }

    public void setVisits(int visits) {
        this.visits = visits;
    }

    public float getDiscount() {
        return discount;
    }
    public void setDiscount(float discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", isActive=" + isActive +
                ", visits=" + visits +
                ", discount=" + discount +
                ", birthday=" + birthday +
                ", isAdmin=" + isAdmin +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (id != user.id) return false;
        if (isActive != user.isActive) return false;
        if (visits != user.visits) return false;
        if (Float.compare(user.discount, discount) != 0) return false;
        if (isAdmin != user.isAdmin) return false;
        if (!email.equals(user.email)) return false;
        if (!password.equals(user.password)) return false;
        if (name != null ? !name.equals(user.name) : user.name != null) return false;
        if (lastName != null ? !lastName.equals(user.lastName) : user.lastName != null) return false;
        return birthday != null ? birthday.equals(user.birthday) : user.birthday == null;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + email.hashCode();
        result = 31 * result + password.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (isActive ? 1 : 0);
        result = 31 * result + visits;
        result = 31 * result + (discount != +0.0f ? Float.floatToIntBits(discount) : 0);
        result = 31 * result + (birthday != null ? birthday.hashCode() : 0);
        result = 31 * result + (isAdmin ? 1 : 0);
        return result;
    }
}
