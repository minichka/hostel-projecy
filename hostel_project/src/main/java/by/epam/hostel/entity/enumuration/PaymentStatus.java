package by.epam.hostel.entity.enumuration;

/**
 * Created by NotePad.by on 21.08.2016.
 */
public enum PaymentStatus {
    booked("booked"),
    fully_payed("fully_payed");

    private String status;

    PaymentStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return status;
    }
}
