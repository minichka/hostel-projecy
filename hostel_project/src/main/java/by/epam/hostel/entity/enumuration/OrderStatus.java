package by.epam.hostel.entity.enumuration;

/**
 * Created by NotePad.by on 13.09.2016.
 */
public enum OrderStatus {
    cancelled("cancelled"),
    confirmed("confirmed"),
    processing("processing");

    private String status;

    OrderStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return status;
    }
}
