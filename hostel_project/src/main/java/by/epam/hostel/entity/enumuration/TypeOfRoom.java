package by.epam.hostel.entity.enumuration;

/**
 * Created by NotePad.by on 21.08.2016.
 */
public enum TypeOfRoom{
        TWO_BEDS(2, "TWO_BEDS"),
        THREE_BEDS(3, "THREE_BEDS"),
        SIX_BEDS(6, "SIX_BEDS");

        private int amount;
        private String type;
        TypeOfRoom(int i, String type){
            this.amount = i;
            this.type = type;
        }

    public static TypeOfRoom findByAmount(int amount){
        TypeOfRoom typeOfRoom = null;
        switch(amount){
            case 2:
                typeOfRoom = TypeOfRoom.TWO_BEDS;
                break;
            case 3:
                typeOfRoom = TypeOfRoom.THREE_BEDS;
                break;
            case 6:
                typeOfRoom = TypeOfRoom.SIX_BEDS;
                break;
            default:
                break;
        }
        return typeOfRoom;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


        public void setAmount(int ammount) {
            this.amount = ammount;
        }

        @Override
        public String toString() {
            return "Beds in room =" + amount;
        }

    }

