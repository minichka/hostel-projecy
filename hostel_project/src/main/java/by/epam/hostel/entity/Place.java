package by.epam.hostel.entity;

import by.epam.hostel.entity.enumuration.StateOfRoom;
import by.epam.hostel.entity.enumuration.TypeOfRoom;

/**
 * Created by NotePad.by on 15.08.2016.
 */
public class Place extends Entity {
    private int id;
    private TypeOfRoom typeOfRoom;
    private float price;
    private boolean isFree;

    public Place() {
    }

    public Place(TypeOfRoom typeOfRoom, int id, float price, boolean stateOfRoom) {
        this.typeOfRoom = typeOfRoom;
        this.id = id;
        this.price = price;
        this.isFree = stateOfRoom;
    }


    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public boolean getStateOfRoom() {
        return isFree;
    }

    public void setStateOfRoom(boolean stateOfRoom) {
        this.isFree = stateOfRoom;
    }

    public TypeOfRoom getTypeOfRoom() {
        return typeOfRoom;
    }

    public void setTypeOfRoom(TypeOfRoom typeOfRoom) {
        this.typeOfRoom = typeOfRoom;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Place place = (Place) o;

        if (id != place.id) return false;
        if (Float.compare(place.price, price) != 0) return false;
        if (isFree != place.isFree) return false;
        return typeOfRoom == place.typeOfRoom;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + typeOfRoom.hashCode();
        result = 31 * result + (price != +0.0f ? Float.floatToIntBits(price) : 0);
        result = 31 * result + (isFree ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Place: " +
                "id=" + id +
                ", typeOfRoom=" + typeOfRoom.toString() +
                ", price=" + price +
                ", is free=" + isFree;
    }

}

