package by.epam.hostel.entity.enumuration;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

/**
 * Created by NotePad.by on 21.08.2016.
 */
public enum TypeOfDiscount implements SQLData{
    NEW_USER("NEW_USER"),
    BIRTHDAY("BIRTHDAY"),
    TWO_VISITS(2,"TWO_VISITS"),
    FIVE_VISITS(5, "FIVE_VISITS"),
    TEN_VISITS(10, "TEN_VISITS"),
    THREE_DAYS(3L, "THREE_DAYS"),
    FIVE_DAYS(5L, "FIVE_DAYS"),
    TEN_DAYS(10L, "TEN_DAYS");

    private String type;
    private int visits;
    private long days;
    private String sql_type;
    TypeOfDiscount(String type) {
        this.type = type;
    }
    TypeOfDiscount(int visits, String type) {
        this.visits = visits;
        this.type = type;
    }

    TypeOfDiscount(long days, String type) {
        this.days = days;
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getVisits() {
        return visits;
    }

    public void setVisits(int visits) {
        this.visits = visits;
    }

    public long getDays() {
        return days;
    }

    public void setDays(long days) {
        this.days = days;
    }

    @Override
    public String toString() {
        return "Type of discount " +
                "visits= " + visits +
                ", days= " + days;
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return sql_type;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        sql_type = typeName;
        type = stream.readString();
        days = stream.readLong();
        visits = stream.readInt();

    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {

        stream.writeInt(visits);
        stream.writeLong(days);
        stream.writeString(type);
    }
}
