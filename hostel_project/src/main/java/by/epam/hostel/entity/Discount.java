package by.epam.hostel.entity;

import by.epam.hostel.entity.enumuration.TypeOfDiscount;

/**
 * Created by NotePad.by on 15.08.2016.
 */
public class Discount extends Entity {
    private int id;
    private TypeOfDiscount typeOfDiscount;
    private float discount;

    public Discount() {
    }

    public Discount(int id, TypeOfDiscount typeOfDiscount, float discount) {
        this.id = id;
        this.typeOfDiscount = typeOfDiscount;
        this.discount = discount;
    }

    public Discount(TypeOfDiscount typeOfDiscount) {
        this.typeOfDiscount = typeOfDiscount;
    }

    public TypeOfDiscount getTypeOfDiscount() {
        return typeOfDiscount;
    }

    public void setTypeOfDiscount(TypeOfDiscount typeOfDiscount) {
        this.typeOfDiscount = typeOfDiscount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Discount discount1 = (Discount) o;

        if (id != discount1.id) return false;
        if (Float.compare(discount1.discount, discount) != 0) return false;
        return typeOfDiscount == discount1.typeOfDiscount;

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + typeOfDiscount.hashCode();
        result = 31 * result + (discount != +0.0f ? Float.floatToIntBits(discount) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Discount: " +
                "id= " + id +
                ", typeOfDiscount= " + typeOfDiscount.toString() +
                ", discount= " + discount;
    }


}


