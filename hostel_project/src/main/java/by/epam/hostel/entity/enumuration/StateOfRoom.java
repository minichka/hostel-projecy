package by.epam.hostel.entity.enumuration;

/**
 * Created by NotePad.by on 21.08.2016.
 */
public enum StateOfRoom{
    FREE("free"),
    BUSY("busy"),
    BOOKED("booked");

    private String state;
    StateOfRoom(String state) {
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "State of room=" + state;
    }
}
