package by.epam.hostel.entity;

import by.epam.hostel.entity.enumuration.OrderStatus;
import by.epam.hostel.entity.enumuration.PaymentStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by NotePad.by on 15.08.2016.
 */
public class Order extends Entity {
    private int id;
    private int usersID;
    private List<Integer> placeID;
    private float discount;
    private Date arrival;
    private Date departure;
    private Date orderDate;
    private float price;
    private PaymentStatus paymentStatus;
    private OrderStatus orderStatus;

    public Order() {
        this.placeID = new ArrayList<Integer>();
    }

    public Order(Date departure, Date arrival, PaymentStatus paymentStatus, int usersID) {
        this.departure = departure;
        this.paymentStatus = paymentStatus;
        this.usersID = usersID;
        this.arrival = arrival;
        this.placeID = new ArrayList<Integer>();
    }

    public Order(Date departure, Date arrival) {
        this.departure = departure;
        this.arrival = arrival;
        this.placeID = new ArrayList<Integer>();
    }

    public Order(int id, int users, List<Integer> places, float discount, Date departure, Date arrival, float price, PaymentStatus paymentStatus) {
        this.id = id;
        this.usersID = users;
        this.placeID = places;
        this.discount = discount;
        this.departure = departure;
        this.arrival = arrival;
        this.price = price;
        this.paymentStatus = paymentStatus;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public int getUsersID() {
        return usersID;
    }

    public void setUsersID(int usersID) {
        this.usersID = usersID;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public List<Integer> getPlaceID() {
        return placeID;
    }

    public void setPlaceID(List<Integer> placeID) {
        this.placeID = placeID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public Date getArrival() {
        return arrival;
    }

    public void setArrival(Date arrival) {
        this.arrival = arrival;
    }

    public Date getDeparture() {
        return departure;
    }

    public void setDeparture(Date departure) {
        this.departure = departure;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }


}
