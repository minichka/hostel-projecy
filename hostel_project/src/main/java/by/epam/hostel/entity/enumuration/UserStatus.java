package by.epam.hostel.entity.enumuration;

/**
 * Created by NotePad.by on 21.08.2016.
 */
public enum UserStatus{
    active("Active"),
    blocked("Blocked");
    private String status;

    UserStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "status= " + status;
    }
}
