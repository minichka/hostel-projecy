package by.epam.hostel.util;
import org.apache.commons.codec.digest.DigestUtils;


/**
 * Created by NotePad.by on 29.08.2016.
 */
public class Coder {
    public  static String hashmd5(String text){
        return DigestUtils.md5Hex(text);
    }
}
