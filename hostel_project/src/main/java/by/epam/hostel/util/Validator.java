package by.epam.hostel.util;

import by.epam.hostel.resource.MessageManager;
import by.epam.hostel.service.UserService;
import java.util.Date;


public class Validator {

    private static Validator instance = new Validator();

    public static Validator getInstance() {
        return instance;
    }

    public String orderCheckEmptyDate(String [] datearr){
        for (String str : datearr)
            if (str.isEmpty())
                return MessageManager.ORDER_EMPTY_DATE;
        return null;
    }
    public String orderCheckEmptyOrder(int [] datearr){
        for (int str : datearr)
            if (str != 0)
                return null;
        return MessageManager.ORDER_EMPTY;

    }

    public String orderDate(Date arrival, Date departure){
        Date today = new Date();
        if(!arrival.after(today))
            return MessageManager.ORDER_DATE_ARRIVAL_ERROR;
        if(!departure.after(arrival))
            return MessageManager.ORDER_DATE_DEPARTURE_ERROR;
        return null;
    }
    public static boolean isExistUser(String email){
        UserService userService = UserService.getInstance();
        return userService.isExist(email);
    }
}
