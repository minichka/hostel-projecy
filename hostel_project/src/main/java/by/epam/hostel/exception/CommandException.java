package by.epam.hostel.exception;

/**
 * Created by NotePad.by on 29.08.2016.
 */
public class CommandException extends Exception {
    public CommandException() {
        super();
    }

    public CommandException(String message) {
        super(message);
    }

    public CommandException(String message, Exception cause) {
        super(message, cause);
    }

    public CommandException(Exception cause) {
        super(cause);
    }
}
