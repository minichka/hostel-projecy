package by.epam.hostel.exception;

/**
 * Created by NotePad.by on 22.08.2016.
 */
public class ConnectionPoolException extends Exception {
    public ConnectionPoolException() {
    }

    public ConnectionPoolException(String message) {
        super(message);
    }

    public ConnectionPoolException(String message, Exception cause) {
        super(message, cause);
    }

    public ConnectionPoolException(Exception cause) {
        super(cause);
    }
}
