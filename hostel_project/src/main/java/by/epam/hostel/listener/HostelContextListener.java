package by.epam.hostel.listener;

import by.epam.hostel.dao.pool.DBConnectionPool;
import by.epam.hostel.exception.ConnectionPoolException;
import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.sql.SQLException;

/**
 * Created by NotePad.by on 28.09.2016.
 */
public class HostelContextListener implements ServletContextListener {
    public static final Logger log = Logger.getLogger(HostelContextListener.class);
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        log.info("HostelServletContextListener.contextInit()");
        try {
            DBConnectionPool.getInstance().initializeAvailableConnection();
        } catch (ConnectionPoolException e) {
            throw new RuntimeException("Error while initializing connection pool", e);
        }


    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        log.info("HostelServletContextListener.contextDestroyed()");
        try {
            DBConnectionPool.getInstance().destroyConnectionPool();
        } catch (ConnectionPoolException e) {
            log.error(e);
        }
    }
}
