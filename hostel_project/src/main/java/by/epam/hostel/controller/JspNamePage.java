package by.epam.hostel.controller;

/**
 * Created by NotePad.by on 29.08.2016.
 */
public class JspNamePage {
    private JspNamePage(){}

    public static final String USER_PAGE ="jsp/user.jsp";
    public static final String ADMIN_PAGE ="jsp/admin.jsp";
    public static final String MAIN_PAGE ="jsp/main.jsp";
    public static final String ERROR_PAGE ="error.jsp";

}
