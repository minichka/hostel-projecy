package by.epam.hostel.controller;

/**
 * Created by NotePad.by on 01.09.2016.
 */
public class SessionParameterName {
    private SessionParameterName() {
    }

    public static final String LOGGED_USER_STATUS = "logged_user_status";
    public static final String LOGGED_USER_ID = "logged_user_id";

}
