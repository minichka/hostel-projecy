package by.epam.hostel.controller;

/**
 * Created by NotePad.by on 29.08.2016.
 */
public class RequestParameterName {
    private RequestParameterName() {}

    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String USER_ID = "user_id";
    public static final String USER = "user";
    public static final String FEEDBACK = "feedback";

    public final static String MESSAGE = "message";
    public final static String LOCALE = "locale";
    public static final String USER_NAME = "firstName";
    public static final String USER_LAST_NAME = "lastName";
    public static final String USER_BIRTHDAY = "birthday";
    public static final String USER_STATUS = "status";

    public static final String FREE_TWO_BEDS_ROOM = "free_two_beds";
    public static final String FREE_THREE_BEDS_ROOM = "free_three_beds";
    public static final String FREE_SIX_BEDS_ROOM = "free_six_beds";
    public static final String ORDER_SIX_BEDS_ROOM = "6_beds_order";
    public static final String ORDER_TWO_BEDS_ROOM = "2_beds_order";
    public static final String ORDER_THREE_BEDS_ROOM = "3_beds_order";
    public static final String ORDER_DATA_ARRIVAL = "date_arrival";
    public static final String ORDER_DATA_DEPARTURE = "date_departure";
    public static final String ORDER_PAYMENT = "payment";
    public static final String ORDER_AMOUNT_OF_SIX_ROOMS = "amount_of_six_rooms";
    public static final String ORDER_AMOUNT_OF_THREE_ROOMS = "amount_of_three_rooms";
    public static final String ORDER_AMOUNT_OF_TWO_ROOMS = "amount_of_two_rooms";
    public static final String ORDER_STATUS = "order_status";
    public static final String ORDER_ID = "order_id";


    public static final String USERS = "users";
    public static final String ORDERS = "orders";


    public static final String SUBMIT = "submit";
    public static final String INFO = "info";
    public static final String URL = "url";

    public static final String COMMAND_NAME = "command";

}
