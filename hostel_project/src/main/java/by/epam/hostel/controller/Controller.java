package by.epam.hostel.controller;

import by.epam.hostel.command.CommandHelper;
import by.epam.hostel.command.ICommand;
import by.epam.hostel.exception.CommandException;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by NotePad.by on 14.08.2016.
 */
public class Controller extends HttpServlet {
    public static String paginationValue;
    Logger log = Logger.getLogger(Controller.class);
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.info("doGet");
        processRequest(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        log.info("doPost");
        processRequest(req,resp);
    }
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        log.info("processRequest");
//        request.setCharacterEncoding("UTF-8");
//        response.setCharacterEncoding("UTF-8");
//        response.setContentType ("text/html; charset=UTF-8");
        String commandName = request.getParameter(RequestParameterName.COMMAND_NAME);
        String page = null;
        ICommand command = null;
        try {
            if (commandName != null) {
                paginationValue = commandName;
                command = CommandHelper.getInstance().getCommand(commandName);
            }
            else {
                command = CommandHelper.getInstance().getCommand(paginationValue);
            }
            page = command.execute(request);
        } catch (CommandException e) {
            page = JspNamePage.ERROR_PAGE;
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher(page);
        if (dispatcher != null) {
            dispatcher.forward(request, response);
        } else {
            errorMessageDirectlyFromResponse(response);
        }
    }

    private void errorMessageDirectlyFromResponse(HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        response.getWriter().println("E R R O RR");
    }
}
