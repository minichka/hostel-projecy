package by.epam.hostel.resource;

/**
 * Created by NotePad.by on 29.08.2016.
 */
public class MessageManager {
    public static final String EMAIL_ERROR = "message.email_error";
    public static final String PASSWORD_ERROR = "message.password_error";
    public static final String REGISTER_ERROR = "message.register_error";
    public static final String SIGNUP_EXIST_ERROR = "message.sign_exist_error";
    public static final String SIGNUP_SUCCESS = "message.sign_success";
    public static final String DATABASE_ERROR = "message.database_error";
    public static final String LOGIN_EMPTY_FIELD = "message.login_empty_field";
    public static final String LOGIN_ERROR = "message.login_error";
    public static final String LOGIN_BL_STATUS = "message.login_bl_status";
    public static final String ORDER_EMPTY_DATE = "message.empty_date";
    public static final String ORDER_EMPTY = "message.empty";
    public static final String ORDER_SUCCESS = "message.order_success";
    public static final String LOGIN_WRONG_PASSWORD = "message.wrong_password";
    public static final String ORDER_DATE_ARRIVAL_ERROR = "message.order_date_arrival_error";
    public static final String ORDER_DATE_DEPARTURE_ERROR = "message.order_date_departure_error";
    public static final String ORDER_PAYMENT_EMPTY = "message.payment_empty";



}
