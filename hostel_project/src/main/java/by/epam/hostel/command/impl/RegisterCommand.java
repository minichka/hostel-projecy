package by.epam.hostel.command.impl;

import by.epam.hostel.command.ICommand;
import by.epam.hostel.controller.JspNamePage;
import by.epam.hostel.controller.RequestParameterName;
import by.epam.hostel.dao.UserDAO;
import by.epam.hostel.dao.impl.UserDAOImpl;
import by.epam.hostel.entity.User;
import by.epam.hostel.exception.CommandException;
import by.epam.hostel.exception.DAOException;
import by.epam.hostel.resource.MessageManager;
import by.epam.hostel.util.Coder;
import by.epam.hostel.util.Validator;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
public class RegisterCommand implements ICommand {
    private static final Logger log = Logger.getLogger(RegisterCommand.class);

    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        log.info("Register Command");
        String page = null;
        try {
            UserDAO userDAO = UserDAOImpl.getInstance();
            String email = request.getParameter(RequestParameterName.EMAIL).trim();
            String password = request.getParameter(RequestParameterName.PASSWORD).trim();
            String name = request.getParameter(RequestParameterName.USER_NAME).trim();
            String lastName = request.getParameter(RequestParameterName.USER_LAST_NAME).trim();

            if(Validator.isExistUser(email)){
                request.setAttribute(RequestParameterName.MESSAGE, MessageManager.SIGNUP_EXIST_ERROR);
                return JspNamePage.MAIN_PAGE;
            }
            User user = new User(email,password,true,false,name, lastName);
            user.setId(Math.abs(user.hashCode()));
                user.setPassword(Coder.hashmd5(password));
                boolean result = userDAO.signUp(user);
                if (!result) {
                    request.setAttribute(RequestParameterName.MESSAGE, MessageManager.SIGNUP_EXIST_ERROR);
                    page = JspNamePage.MAIN_PAGE;
                }else{
                    request.getSession().setAttribute(RequestParameterName.USER, user);
                    request.setAttribute(RequestParameterName.MESSAGE,MessageManager.SIGNUP_SUCCESS);
                    page = JspNamePage.MAIN_PAGE;
                }

        }catch (DAOException e) {
            log.error(e);
            request.setAttribute(RequestParameterName.MESSAGE, MessageManager.DATABASE_ERROR);
            page = JspNamePage.ERROR_PAGE;
            return page;
        }
        request.getSession().setAttribute(RequestParameterName.URL,page);
        return page;

    }
}
