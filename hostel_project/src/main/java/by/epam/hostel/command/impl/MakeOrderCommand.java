package by.epam.hostel.command.impl;

import by.epam.hostel.command.ICommand;
import by.epam.hostel.controller.JspNamePage;
import by.epam.hostel.controller.RequestParameterName;
import by.epam.hostel.controller.SessionParameterName;
import by.epam.hostel.entity.Order;
import by.epam.hostel.entity.enumuration.PaymentStatus;
import by.epam.hostel.exception.CommandException;
import by.epam.hostel.exception.DAOException;
import by.epam.hostel.resource.MessageManager;
import by.epam.hostel.service.OrderService;
import by.epam.hostel.util.Validator;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.Date;
import java.text.SimpleDateFormat;

public class MakeOrderCommand implements ICommand {
    Logger log = Logger.getLogger(MakeOrderCommand.class);
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        log.info("MakeOrderCommand.execute");
        String page = null;
        Date dateOfArrival = null;
        Date dateOfDeparture = null;
        OrderService orderService = new OrderService();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String [] dateArray = new String[]{request.getParameter(RequestParameterName.ORDER_DATA_ARRIVAL),request.getParameter(RequestParameterName.ORDER_DATA_DEPARTURE)};
        String mypage = (String) request.getSession().getAttribute(RequestParameterName.URL);
        if(Validator.getInstance().orderCheckEmptyDate(dateArray) != null) {
            request.setAttribute(RequestParameterName.MESSAGE, Validator.getInstance().orderCheckEmptyDate(dateArray));
            if (mypage.contains("user"))
                return new ToUserPageCommand().execute(request);
            else
                return new ToAdminPageCommand().execute(request);

        }
        try {
            dateOfArrival = simpleDateFormat.parse(dateArray[0]);
            dateOfDeparture = simpleDateFormat.parse(dateArray[1]);
            if(Validator.getInstance().orderDate(dateOfArrival,dateOfDeparture) != null) {
                request.setAttribute(RequestParameterName.MESSAGE, Validator.getInstance().orderDate(dateOfArrival,dateOfDeparture));
                if (mypage.contains("user"))
                    return new ToUserPageCommand().execute(request);
                else
                    return new ToAdminPageCommand().execute(request);
            }

           } catch (ParseException e) {
            log.info("date parse error");
           throw  new CommandException();
       }

        int [] amountOfRooms = new int[7];
        if(!request.getParameter(RequestParameterName.ORDER_AMOUNT_OF_SIX_ROOMS).isEmpty())
            amountOfRooms[6] = Integer.parseInt(request.getParameter(RequestParameterName.ORDER_AMOUNT_OF_SIX_ROOMS));
        if(!request.getParameter(RequestParameterName.ORDER_AMOUNT_OF_THREE_ROOMS).isEmpty())
            amountOfRooms[3] = Integer.parseInt(request.getParameter(RequestParameterName.ORDER_AMOUNT_OF_THREE_ROOMS));
        if(!request.getParameter(RequestParameterName.ORDER_AMOUNT_OF_TWO_ROOMS).isEmpty())
            amountOfRooms[2] = Integer.parseInt(request.getParameter(RequestParameterName.ORDER_AMOUNT_OF_TWO_ROOMS));
        if(Validator.getInstance().orderCheckEmptyOrder(amountOfRooms) != null){
            request.setAttribute(RequestParameterName.MESSAGE, Validator.getInstance().orderCheckEmptyOrder(amountOfRooms));
            if (mypage.contains("user"))
                return new ToUserPageCommand().execute(request);
            else
                return new ToAdminPageCommand().execute(request);
        }

        if ((boolean)request.getSession(true).getAttribute(SessionParameterName.LOGGED_USER_STATUS)){
            Order order = new Order(dateOfDeparture,dateOfArrival);
            try {
                orderService.MakeOrder(amountOfRooms,order,true);
            } catch (DAOException e) {
                e.printStackTrace();
            }
        }
        else{
            try {
                String payment = request.getParameter(RequestParameterName.ORDER_PAYMENT);
                if (payment == null){
                    request.setAttribute(RequestParameterName.MESSAGE, MessageManager.ORDER_PAYMENT_EMPTY);
                    if (mypage.contains("user"))
                        return new ToUserPageCommand().execute(request);
                    else
                        return new ToAdminPageCommand().execute(request);
                }
                int userId = Integer.parseInt(request.getSession(true).getAttribute(SessionParameterName.LOGGED_USER_ID).toString());
                Order order = new Order(dateOfDeparture, dateOfArrival, PaymentStatus.valueOf(payment),userId);
                orderService.MakeOrder(amountOfRooms, order,false);
            } catch (DAOException e) {
                e.printStackTrace();
            }
        }

        page = JspNamePage.MAIN_PAGE;
        request.setAttribute(RequestParameterName.MESSAGE, MessageManager.ORDER_SUCCESS);
        request.getSession().setAttribute(RequestParameterName.URL,page);
        return page;
    }
}
