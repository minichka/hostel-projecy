package by.epam.hostel.command.impl;

import by.epam.hostel.command.ICommand;
import by.epam.hostel.controller.JspNamePage;
import by.epam.hostel.exception.CommandException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by NotePad.by on 29.08.2016.
 */
public class EmptyCommand implements ICommand {
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        return JspNamePage.ERROR_PAGE;
    }
}
