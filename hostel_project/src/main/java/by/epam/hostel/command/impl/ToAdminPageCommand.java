package by.epam.hostel.command.impl;

import by.epam.hostel.command.ICommand;
import by.epam.hostel.controller.JspNamePage;
import by.epam.hostel.controller.RequestParameterName;
import by.epam.hostel.dao.OrderDAO;
import by.epam.hostel.dao.UserDAO;
import by.epam.hostel.dao.impl.OrderDAOImpl;
import by.epam.hostel.dao.impl.PlaceDAOImpl;
import by.epam.hostel.dao.impl.UserDAOImpl;
import by.epam.hostel.entity.enumuration.TypeOfRoom;
import by.epam.hostel.exception.CommandException;
import by.epam.hostel.exception.DAOException;
import by.epam.hostel.service.OrderService;
import by.epam.hostel.service.PlaceService;
import by.epam.hostel.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by NotePad.by on 01.09.2016.
 */
public class ToAdminPageCommand implements ICommand {
    Logger log = Logger.getLogger(ToAdminPageCommand.class);
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        log.info("ToAdminPageCommand.execute");
        String pageStr = null;
        int page = 1;
        int recordsPerPage = 2;
        if(request.getParameter("page") != null)
            page = Integer.parseInt(request.getParameter("page"));
        PlaceService placeService = PlaceService.getInstance();
        OrderService orderService = OrderService.getInstance();
        UserService userService = UserService.getInstance();
            request.setAttribute(RequestParameterName.FREE_SIX_BEDS_ROOM, placeService.countFreeRooms(TypeOfRoom.SIX_BEDS, true));
            request.setAttribute(RequestParameterName.FREE_THREE_BEDS_ROOM, placeService.countFreeRooms(TypeOfRoom.THREE_BEDS, true));
            request.setAttribute(RequestParameterName.FREE_TWO_BEDS_ROOM, placeService.countFreeRooms(TypeOfRoom.TWO_BEDS, true));
            request.setAttribute(RequestParameterName.USERS, userService.findUserByRole(false));
            request.setAttribute(RequestParameterName.ORDERS, orderService.pageViewAll((page-1)*recordsPerPage,recordsPerPage));
            int noOfRecords = orderService.getNoOfRecords();
            int noOfPages = (int) Math.ceil(noOfRecords * 1.0 / recordsPerPage);
            request.setAttribute("noOfPages", noOfPages);
            request.setAttribute("currentPage", page);
        pageStr = JspNamePage.ADMIN_PAGE;
        request.getSession().setAttribute(RequestParameterName.URL, pageStr);

        return pageStr;
    }
}
