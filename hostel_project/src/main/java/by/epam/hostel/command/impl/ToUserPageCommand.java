package by.epam.hostel.command.impl;

import by.epam.hostel.command.ICommand;
import by.epam.hostel.controller.JspNamePage;
import by.epam.hostel.controller.RequestParameterName;
import by.epam.hostel.controller.SessionParameterName;
import by.epam.hostel.dao.impl.OrderDAOImpl;
import by.epam.hostel.entity.enumuration.TypeOfRoom;
import by.epam.hostel.exception.CommandException;
import by.epam.hostel.service.PlaceService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class ToUserPageCommand implements ICommand {
    Logger log = Logger.getLogger(ToUserPageCommand.class);
    @Override
    public String execute(HttpServletRequest request) throws CommandException{
        PlaceService placeService = PlaceService.getInstance();
        String page;
            request.setAttribute(RequestParameterName.FREE_SIX_BEDS_ROOM, placeService.countFreeRooms(TypeOfRoom.SIX_BEDS, true));
            request.setAttribute(RequestParameterName.FREE_THREE_BEDS_ROOM, placeService.countFreeRooms(TypeOfRoom.THREE_BEDS, true));
            request.setAttribute(RequestParameterName.FREE_TWO_BEDS_ROOM, placeService.countFreeRooms(TypeOfRoom.TWO_BEDS, true));
            log.info(request.getSession(true).getAttribute(SessionParameterName.LOGGED_USER_ID));
            int id = (int) request.getSession(true).getAttribute(SessionParameterName.LOGGED_USER_ID);
            request.setAttribute(RequestParameterName.ORDERS, OrderDAOImpl.getInstance().findOrderByUserID(id));
        page = JspNamePage.USER_PAGE;
        request.getSession().setAttribute(RequestParameterName.URL, page);
        return page;
    }
}
