package by.epam.hostel.command.impl;

import by.epam.hostel.command.ICommand;
import by.epam.hostel.controller.RequestParameterName;
import by.epam.hostel.dao.UserDAO;
import by.epam.hostel.dao.impl.UserDAOImpl;
import by.epam.hostel.exception.CommandException;
import by.epam.hostel.exception.DAOException;
import by.epam.hostel.service.UserService;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by NotePad.by on 11.09.2016.
 */
public class BlockUserCommand implements ICommand {
    Logger log = Logger.getLogger(BlockUserCommand.class);
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        log.info("BlockUserCommand.execute()");
        String page = null;
        UserService userService = UserService.getInstance();
            boolean status= !Boolean.parseBoolean(request.getParameter(RequestParameterName.USER_STATUS));
            log.info(status);
            userService.changeStatus(request.getParameter(RequestParameterName.EMAIL),status);
        page = new ToAdminPageCommand().execute(request);
        request.getSession().setAttribute(RequestParameterName.URL, page);
        return page;
    }
}
