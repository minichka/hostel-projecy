package by.epam.hostel.command;

import by.epam.hostel.command.impl.*;
import by.epam.hostel.exception.CommandException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by NotePad.by on 29.08.2016.
 */
public final class CommandHelper {
    private static final CommandHelper instance = new CommandHelper();
    private final Map<CommandName, ICommand> commands = new HashMap<>();

    public CommandHelper() {
        commands.put(CommandName.REGISTER, new RegisterCommand());
        commands.put(CommandName.NO_SUCH_COMMAND, new EmptyCommand());
        commands.put(CommandName.LOG_IN, new LogInCommand());
        commands.put(CommandName.LOG_OUT, new LogOutCommand());
        commands.put(CommandName.TO_ADMIN_PAGE, new ToAdminPageCommand());
        commands.put(CommandName.TO_MAIN_PAGE, new ToMainPageCommand());
        commands.put(CommandName.TO_USER_PAGE, new ToUserPageCommand());
        commands.put(CommandName.MAKE_ORDER, new MakeOrderCommand());
        commands.put(CommandName.BLOCK_USER, new BlockUserCommand());
        commands.put(CommandName.CHANGE_ORDER_STATUS, new ChangeOrderStatusCommand());
        commands.put(CommandName.CHANGE_LOCALE, new ChangeLocaleCommand());
    }

    public static CommandHelper getInstance() {
        return instance;
    }

    public ICommand getCommand(String commandName) throws CommandException{
        CommandName name;
        try{
            name = CommandName.valueOf(commandName.toUpperCase());
        }catch(IllegalArgumentException | NullPointerException e){
            throw new CommandException("unknown command", e);
        }

        ICommand command;
        if ( null != name){
            command = commands.get(name);
        } else{
            command = commands.get(CommandName.NO_SUCH_COMMAND);
        }

        return command;
    }
}
