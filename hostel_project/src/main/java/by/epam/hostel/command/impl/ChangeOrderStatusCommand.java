package by.epam.hostel.command.impl;

import by.epam.hostel.command.ICommand;
import by.epam.hostel.controller.RequestParameterName;
import by.epam.hostel.controller.SessionParameterName;
import by.epam.hostel.dao.OrderDAO;
import by.epam.hostel.dao.impl.OrderDAOImpl;
import by.epam.hostel.entity.enumuration.OrderStatus;
import by.epam.hostel.exception.CommandException;

import javax.servlet.http.HttpServletRequest;

import by.epam.hostel.service.OrderService;
import org.apache.log4j.Logger;

/**
 * Created by NotePad.by on 13.09.2016.
 */
public class ChangeOrderStatusCommand implements ICommand{
    Logger log = Logger.getLogger(ChangeOrderStatusCommand.class);
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        log.info("ChangeOrderStatusCommand");
        String page = null;
        //OrderDAO orderDAO = OrderDAOImpl.getInstance();
        OrderService orderService = OrderService.getInstance();
        int orderId = Integer.parseInt(request.getParameter(RequestParameterName.ORDER_ID));
        OrderStatus orderStatus = OrderStatus.valueOf(request.getParameter(RequestParameterName.ORDER_STATUS));
        log.info(orderStatus.toString());
        orderService.changeOrderStatus(orderId,orderStatus);
        boolean isAdmin = (boolean) request.getSession(true).getAttribute(SessionParameterName.LOGGED_USER_STATUS);
        if (isAdmin)
            page = new ToAdminPageCommand().execute(request);
        else
            page = new ToUserPageCommand().execute(request);
        return page;
    }
}
