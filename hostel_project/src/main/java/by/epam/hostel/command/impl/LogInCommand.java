package by.epam.hostel.command.impl;

import by.epam.hostel.command.CommandName;
import by.epam.hostel.command.ICommand;
import by.epam.hostel.controller.Controller;
import by.epam.hostel.controller.JspNamePage;
import by.epam.hostel.controller.RequestParameterName;
import by.epam.hostel.controller.SessionParameterName;
import by.epam.hostel.entity.User;
import by.epam.hostel.exception.CommandException;
import by.epam.hostel.resource.MessageManager;
import by.epam.hostel.service.UserService;
import by.epam.hostel.util.Coder;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletRequest;

public class LogInCommand implements ICommand {
    private static final Logger log = Logger.getLogger(LogInCommand.class);
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        log.info("LogInCommand");
        String page = null;
        String email = request.getParameter(RequestParameterName.EMAIL);
        String password = request.getParameter(RequestParameterName.PASSWORD);
//        if (!Validator.checkEmptyField(email, password)){
//            request.setAttribute(RequestParameterName.MESSAGE, MessageManager.LOGIN_EMPTY_FIELD);
//            page = JspNamePage.MAIN_PAGE;
//            return page;
//        }
//        String message = Validator.checkLoginInfo(password,email);
//        if (message != null){
//            request.setAttribute(RequestParameterName.MESSAGE,message);
//            page = JspNamePage.MAIN_PAGE;
//            return page;
//        }
        UserService userService = UserService.getInstance();
            User user = userService.findUserByEmail(email);
            if (user == null){
                request.setAttribute(RequestParameterName.MESSAGE, MessageManager.LOGIN_ERROR);
                page = JspNamePage.MAIN_PAGE;
            }else if (!user.isActive()){
                request.setAttribute(RequestParameterName.MESSAGE,MessageManager.LOGIN_BL_STATUS);
                page = JspNamePage.MAIN_PAGE;
            }else if (!user.getPassword().equals(Coder.hashmd5(password))){
                request.setAttribute(RequestParameterName.MESSAGE, MessageManager.LOGIN_WRONG_PASSWORD);
                page = JspNamePage.MAIN_PAGE;
            }
            else{
                request.setAttribute(RequestParameterName.USER, user);
                request.getSession(true).setAttribute(SessionParameterName.LOGGED_USER_ID, user.getId());
                request.getSession(true).setAttribute(SessionParameterName.LOGGED_USER_STATUS, user.isAdmin());
                if (user.isAdmin()) {
                    page = new ToAdminPageCommand().execute(request);
                    request.getSession().setAttribute("role","admin");
                    Controller.paginationValue = CommandName.TO_ADMIN_PAGE.toString();
                }
                else {
                    page = new ToUserPageCommand().execute(request);
                    request.getSession().setAttribute("role","user");
                    Controller.paginationValue = CommandName.TO_USER_PAGE.toString();
                }
            }
        //request.getSession().setAttribute(RequestParameterName.URL, page);
        return page;
    }
}
