package by.epam.hostel.command.impl;

import by.epam.hostel.command.ICommand;
import by.epam.hostel.controller.JspNamePage;
import by.epam.hostel.controller.RequestParameterName;
import by.epam.hostel.exception.CommandException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by NotePad.by on 01.09.2016.
 */
public class ToMainPageCommand implements ICommand {
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        String page;
        page = JspNamePage.MAIN_PAGE;
        request.getSession().setAttribute(RequestParameterName.URL, page);
        return page;
    }
}
