package by.epam.hostel.command;

import by.epam.hostel.exception.CommandException;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by NotePad.by on 29.08.2016.
 */
public interface ICommand {
    public String execute(HttpServletRequest request) throws CommandException;
}
