package by.epam.hostel.command;

/**
 * Created by NotePad.by on 29.08.2016.
 */
public enum CommandName {
    REGISTER,
    NO_SUCH_COMMAND,
    LOG_IN,
    LOG_OUT,
    TO_MAIN_PAGE,
    TO_USER_PAGE,
    TO_ADMIN_PAGE,
    MAKE_ORDER,
    BLOCK_USER,
    CHANGE_ORDER_STATUS,
    LEAVE_FEEDBACK,
    CHANGE_LOCALE
}
