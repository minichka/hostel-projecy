package by.epam.hostel.command.impl;

import by.epam.hostel.command.ICommand;
import by.epam.hostel.controller.RequestParameterName;
import by.epam.hostel.controller.SessionParameterName;
import by.epam.hostel.exception.CommandException;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by NotePad.by on 19.09.2016.
 */
public class ChangeLocaleCommand implements ICommand{
    Logger log = Logger.getLogger(ChangeOrderStatusCommand.class);
    @Override
    public String execute(HttpServletRequest request) throws CommandException {
        log.info("ChangeLocaleCommand.execute");
        String page = null;
        log.info(request.getParameter(RequestParameterName.LOCALE));
        request.getSession(true).setAttribute(RequestParameterName.LOCALE, request.getParameter(RequestParameterName.LOCALE));
        log.info(request.getParameter(RequestParameterName.LOCALE));
        if (request.getSession(true).getAttribute(SessionParameterName.LOGGED_USER_ID) == null)
            return new ToMainPageCommand().execute(request);
        page = (String) request.getSession().getAttribute(RequestParameterName.URL);
        if (page.contains("user"))
            return new ToUserPageCommand().execute(request);
        else
            return new ToAdminPageCommand().execute(request);
    }
}
