<%--
  Created by IntelliJ IDEA.
  User: NotePad.by
  Date: 14.08.2016
  Time: 22:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>--%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ctg" uri="customtags"%>
<%@ include file="/jsp/jspf/bundle.jspf"%>
<!DOCTYPE html>

<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><fmt:message key="title.user_page"/></title>

    <style>
        <%@include file="../css/bootstrap.css" %>
        <%@include file="../css/style.css" %>
    </style>

    <script type="text/javascript" src="<c:url value="../js/bootstrap.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="../js/jquery-3.1.0.min.js"/>"></script>
    <script>
        <%@include file="../js/bootstrap.min.js"%>
        <%@include file="../js/jquery-3.1.0.min.js"%>
    </script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.1.0.min.js"></script>
</head>
<body>
<nav id="mainNav">
    <div class="container">
        <div class="row">
            <div class="navbar navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#resposive-menu" >
                            <span class="sr-only">Открыть навигацию</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="" class="navbar-brand"> Hostel</a>
                    </div>
                    <div class="collapse navbar-collapse" id="resposive-menu">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <form action="Controller" method="post">
                                    <input type="hidden" name="command" value="to_main_page">
                                    <button class="btn btn-default" type="submit"><fmt:message key="button.main_page"/></button>
                                </form>
                            </li>
                            <li><a href="#my_order"><fmt:message key="nav.my_orders"/></a></li>
                            <li><a href="#order"><fmt:message key="nav.make_order"/></a></li>
                            <li class="divider"></li>
                            <li>
                                <form class="form" method="POST" action="Controller">
                                    <input type="hidden" name="command" value="change_locale"  />
                                    <button class="locale" type="submit"><img src="../img/ru.png" alt="русский"></button>
                                    <input type="hidden" name="locale" value="ru"/>
                                </form>
                            </li>
                            <li>
                                <form class="form" method="POST" action="Controller">
                                    <input type="hidden" name="command" value="change_locale" />
                                    <button class="locale" type="submit"><img src="../img/en.png" alt="english"></button>
                                    <input type="hidden" name="locale" value="en"/>
                                </form>
                            </li>
                            <li>
                                <form action="Controller" method="post">
                                    <input type="hidden" name="command" value="log_out">
                                    <button class="btn btn-danger" type="submit"><fmt:message key="button.log_out"/></button>
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</nav>
<%@include file="jspf/header.jspf"%>
<section id="my_order">
    <h3><fmt:message key="header.my_orders"/></h3>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr class="head">
                <th><fmt:message key="static.order_number"/></th>
                <th><fmt:message key="static.place_number"/></th>
                <th><fmt:message key="static.date_arrival"/></th>
                <th><fmt:message key="static.date_departure"/></th>
                <th><fmt:message key="static.price"/></th>
                <th><fmt:message key="static.payment_status"/></th>
                <th><fmt:message key="static.order_status"/></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="order" items="${orders}">
                <c:if test="${order.getClass().getSimpleName() eq 'Order'}">
                    <tr>
                        <td><c:out value="${order.getId()}"/></td>
                        <td>
                            <c:forEach var="place" items="${order.getPlaceID()}">
                                <c:out value="${place}"/>
                            </c:forEach>
                        </td>
                        <td><c:out value="${order.getArrival()}"/></td>
                        <td><c:out value="${order.getDeparture()}"/></td>
                        <td><c:out value="${order.getPrice()}"/></td>
                        <td>
                            <c:if test="${order.getPaymentStatus() eq 'booked'}">
                                <fmt:message key="static.booked"/>
                            </c:if>
                            <c:if test="${order.getPaymentStatus() eq 'fully_payed'}">
                                <fmt:message key="static.fully_payed"/>
                            </c:if>
                        </td>
                        <td>
                            <c:if test="${order.getOrderStatus() eq 'cancelled'}">
                                <fmt:message key="static.cancelled"/>
                            </c:if>
                            <c:if test="${order.getOrderStatus() eq 'confirmed'}">
                                <fmt:message key="static.confirmed"/>
                            </c:if>
                            <c:if test="${order.getOrderStatus() eq 'processing'}">
                                <fmt:message key="static.processing"/>
                            </c:if>
                        </td>
                        <td>
                        <c:if test="${order.getOrderStatus() eq 'confirmed'}">
                                <form action="Controller" method="post">
                                    <input type="hidden" name="command" value="change_order_status">
                                    <input type="hidden" name="order_status" value="cancelled">
                                    <input type="hidden" name="order_id" value="${order.getId()}" >
                                    <button class="btn btn-danger"><fmt:message key="button.cancel"/></button>
                                </form>
                        </c:if>
                        </td>
                    </tr>
                </c:if>
            </c:forEach>
            </tbody>
        </table>
    </div>
</section>
<section class="order" id="order">
    <div class="container">
        <div class="header">
            <h3 class="container-title"><fmt:message key="header.make_order"/></h3>
        </div>
        <div class="container-body">
            <form method="post" id="bookform"action="Controller">
                <input type="hidden" name="command" value="make_order"/>
                <h3>
                    <fmt:message key="header.choose_date"/>
                </h3>
                <div class="step_1" id="step_1" >
                    <br>
                    <fmt:message key="header.choose_arrival_date"/>
                    <input type="date" name="date_arrival">
                    <fmt:message key="header.choose_departure_date"/>
                    <input type="date" name="date_departure">
                </div>
                <h3><fmt:message key="header.choose_place"/></h3>
                <div class="step_2" id="step_2">
                    <table class="table table-striped">
                        <thead>
                        <tr class="head">
                            <td class="room"><fmt:message key="static.type_of_room"/></td>
                            <td class="date"><fmt:message key="static.price_and_free_rooms"/></td>
                            <td class="date"><fmt:message key="static.choose_rooms"/></td>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="room">
                                <b><fmt:message key="static.six_bed"/> </b>
                                						</td>
                            <td class="date date-3">
                                <fmt:message key="static.price"/>: <br>
                                <span><fmt:message key="static.free"/>: ${free_six_beds}</span><br><label>
                            </label></td>
                            <td class="date date-3">
                                <fmt:message key="static.number_of_ordered_rooms"/>
                                <input type="number" name="amount_of_six_rooms" min="0" max=${free_six_beds}>
                            </td>
                        </tr>
                        <tr class="gray">
                            <td class="room">
                                <b><fmt:message key="static.three_bed"/></b>
                            </td>
                            <td class="date date-4">
                                <fmt:message key="static.price"/>: <br>
                                <span><fmt:message key="static.free"/>: ${free_three_beds}</span><br>
                                </td>
                            <td class="date date-4">
                                <fmt:message key="static.number_of_ordered_rooms"/>
                                <input type="number" name="amount_of_three_rooms"  min="0"  max=${free_three_beds}>
                            </td>
                        </tr>
                        <tr>
                            <td class="room">
                                <b><fmt:message key="static.two_bed"/></b>
                            </td>
                            <td class="date date-5">
                                <fmt:message key="static.price"/>: <br>
                                <span><fmt:message key="static.free"/>: ${free_two_beds}</span><br>
                            <td class="date date-5">
                                <fmt:message key="static.number_of_ordered_rooms"/>
                                <input type="number" name="amount_of_two_rooms" min="0"  max=${free_two_beds}>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    </div>

                <h3><fmt:message key="header.choose_payment"/> </h3>
                <div class="step_3" id="step_3">
                    <p><label>
                        <input name="payment" type="radio" value="booked">
                    </label> <fmt:message key="radio.booking"/></p>
                    <p><label>
                        <input name="payment" type="radio" value="fully_payed">
                    </label> <fmt:message key="radio.fully_paid"/></p>
                    <button class="btn btn-primary" type="submit"><fmt:message key="button.make_order"/></button>
                </div>
            </form>
        </div>
    </div>
</section>
<%@include file="jspf/footer.jspf"%>
</body>
</html>
