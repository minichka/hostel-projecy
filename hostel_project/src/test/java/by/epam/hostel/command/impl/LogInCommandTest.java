package by.epam.hostel.command.impl;

import by.epam.hostel.controller.RequestParameterName;
import by.epam.hostel.dao.pool.DBConnectionPool;
import by.epam.hostel.exception.ConnectionPoolException;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.junit.Assert.*;

public class LogInCommandTest extends Mockito{
    private static Logger logger = Logger.getLogger(LogInCommandTest.class);
    @BeforeClass
    public static void initializePool() {
        try {
            DBConnectionPool.getInstance().initializeAvailableConnection();
        } catch (ConnectionPoolException e) {
            throw new RuntimeException("Error while initializing connection pool", e);
        }
    }
    @Test
    public void testExecute() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);

        new LogInCommand().execute(request);

        verify(request,atLeast(1)).getParameter(RequestParameterName.EMAIL);
        verify(request,atLeast(1)).getParameter(RequestParameterName.PASSWORD);
    }

    @AfterClass
    public static void destroyPool() {
        try {
            DBConnectionPool.getInstance().destroyConnectionPool();
        } catch (ConnectionPoolException e) {
            logger.error(e);
        }
    }


}