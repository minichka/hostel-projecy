package by.epam.hostel.dao.impl;

import by.epam.hostel.dao.OrderDAO;
import by.epam.hostel.dao.pool.DBConnectionPool;
import by.epam.hostel.entity.Order;
import by.epam.hostel.entity.enumuration.OrderStatus;
import by.epam.hostel.entity.enumuration.PaymentStatus;
import by.epam.hostel.entity.enumuration.TypeOfDiscount;
import by.epam.hostel.exception.ConnectionPoolException;
import org.apache.log4j.Logger;
import org.junit.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.SimpleFormatter;

import static org.junit.Assert.*;

/**
 * Created by NotePad.by on 06.10.2016.
 */
public class OrderDAOImplTest {
    private static Logger logger = Logger.getLogger(OrderDAOImplTest.class);
    OrderDAO orderDAO = OrderDAOImpl.getInstance();

    @BeforeClass
    public static void initializePool() {
        try {
            DBConnectionPool.getInstance().initializeAvailableConnection();
        } catch (ConnectionPoolException e) {
            throw new RuntimeException("Error while initializing connection pool", e);
        }
    }


    @Test
    @Before
    public void testCreateOrder() throws Exception {
        logger.info("testCreateOrder");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        List<Integer> places = new ArrayList();
        places.add(1);
        places.add(2);
        Order order = new Order();
        order.setUsersID(3);
        order.setPlaceID(places);
        order.setArrival(simpleDateFormat.parse("2016-10-07"));
        order.setDeparture(simpleDateFormat.parse("2016-10-17"));
        order.setOrderDate(new Date());
        order.setPrice(30);
        order.setPaymentStatus(PaymentStatus.fully_payed);
        order.setOrderStatus(OrderStatus.processing);
        order.setId(Math.abs(order.hashCode()));

        orderDAO.createOrder(order);
        Order newOrder = orderDAO.findEntityById(order.getId());
        Assert.assertEquals(order.getId(),newOrder.getId());
        Assert.assertEquals(order.getUsersID(),newOrder.getUsersID());
        Assert.assertEquals(order.getPlaceID(),newOrder.getPlaceID());
        Assert.assertEquals(order.getPrice(),newOrder.getPrice(), 0.0001);
        Assert.assertEquals(order.getArrival(),newOrder.getArrival());
        Assert.assertEquals(order.getDeparture(),newOrder.getDeparture());
        Assert.assertEquals(order.getOrderStatus(),newOrder.getOrderStatus());
        Assert.assertEquals(order.getPaymentStatus(),newOrder.getPaymentStatus());
        orderDAO.delete(order.getId());
    }
    @Test
    public void testChangeOrderStatus() throws Exception {
        logger.info("testChangeOrderStatus");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        List<Integer> places = new ArrayList();
        places.add(1);
        places.add(2);
        Order order = new Order();
        order.setUsersID(3);
        order.setPlaceID(places);
        order.setArrival(simpleDateFormat.parse("2016-10-07"));
        order.setDeparture(simpleDateFormat.parse("2016-10-17"));
        order.setOrderDate(new Date());
        order.setPrice(30);
        order.setPaymentStatus(PaymentStatus.fully_payed);
        order.setOrderStatus(OrderStatus.processing);
        order.setId(Math.abs(order.hashCode()));

        orderDAO.createOrder(order);
        orderDAO.changeOrderStatus(order.getId(), OrderStatus.confirmed);
        Order newOrder = orderDAO.findEntityById(order.getId());
        Assert.assertEquals(OrderStatus.confirmed, newOrder.getOrderStatus());
        orderDAO.delete(order.getId());
    }

    @AfterClass
    public static void destroyPool() {
        try {
            DBConnectionPool.getInstance().destroyConnectionPool();
        } catch (ConnectionPoolException e) {
            logger.error(e);
        }
    }
}