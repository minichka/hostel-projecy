package by.epam.hostel.dao.impl;

import by.epam.hostel.dao.UserDAO;
import by.epam.hostel.dao.pool.DBConnectionPool;
import by.epam.hostel.entity.User;
import by.epam.hostel.entity.enumuration.TypeOfDiscount;
import by.epam.hostel.exception.ConnectionPoolException;
import by.epam.hostel.exception.DAOException;
import by.epam.hostel.util.Coder;
import org.apache.log4j.Logger;
import org.junit.*;

import static org.junit.Assert.*;

public class UserDAOImplTest {
    private static Logger logger = Logger.getLogger(UserDAOImplTest.class);
    private static UserDAO userDAO = UserDAOImpl.getInstance();

    @BeforeClass
    public static void initializePool() {
        try {
            DBConnectionPool.getInstance().initializeAvailableConnection();
        } catch (ConnectionPoolException e) {
            throw new RuntimeException("Error while initializing connection pool", e);
        }
    }

    @Test
    @Before
    public void testSignUp() throws DAOException {
        logger.info("testSignUp");
        User user = new User();
        user.setEmail("pupka@gmail.com");
        user.setDiscount(DiscountDAOImpl.getInstance().findDiscountByType(TypeOfDiscount.NEW_USER).getDiscount());
        user.setPassword(Coder.hashmd5("qwerty"));
        user.setName("Елена");
        user.setLastName("Минич");
        user.setAdmin(false);
        user.setActive(true);
        user.setVisits(0);
        user.setId(Math.abs(user.hashCode()));

        userDAO.signUp(user);
        String email = user.getEmail();
        Assert.assertNotNull(email);
        String password = user.getPassword();
        Assert.assertNotNull(password);

        User newUser = userDAO.findUserByEmail(email);
        Assert.assertEquals(user.getId(), newUser.getId());
        Assert.assertEquals(user.getEmail(), newUser.getEmail());
        Assert.assertEquals(user.getPassword(), newUser.getPassword());
        Assert.assertEquals(newUser.getDiscount(), user.getDiscount(),0.001);
        Assert.assertEquals(newUser.getLastName(), user.getLastName());
        Assert.assertEquals(newUser.getName(), user.getName());
        Assert.assertEquals(newUser.isActive(), user.isActive());
        Assert.assertEquals(newUser.isAdmin(), user.isAdmin());
        Assert.assertEquals(newUser.getVisits(), user.getVisits());
    }
    @Test
    public void testFindUserByEmail() throws DAOException {
        logger.info("testFindUserByEmail");
        User user = new User();
        user.setEmail("pupka@gmail.com");
        user.setDiscount(DiscountDAOImpl.getInstance().findDiscountByType(TypeOfDiscount.NEW_USER).getDiscount());
        user.setPassword(Coder.hashmd5("qwerty"));
        user.setName("Елена");
        user.setLastName("Минич");
        user.setAdmin(false);
        user.setActive(true);
        user.setVisits(0);
        user.setId(Math.abs(user.hashCode()));

        User newUser = userDAO.findUserByEmail("pupka@gmail.com");
        Assert.assertEquals(newUser.getId(), user.getId());
        Assert.assertEquals(newUser.getEmail(), user.getEmail());
        Assert.assertEquals(newUser.getPassword(), user.getPassword());
        Assert.assertEquals(newUser.getDiscount(), user.getDiscount(),0.01);
        Assert.assertEquals(newUser.getLastName(), user.getLastName());
        Assert.assertEquals(newUser.getName(), user.getName());
        Assert.assertEquals(newUser.isActive(), user.isActive());
        Assert.assertEquals(newUser.isAdmin(), user.isAdmin());
        Assert.assertEquals(newUser.getVisits(), user.getVisits());
    }
    @Test
    public void testFindEntityById() throws DAOException {
        logger.info("testFindUserByEmail");
        User user = new User();
        user.setEmail("pupka@gmail.com");
        user.setDiscount(DiscountDAOImpl.getInstance().findDiscountByType(TypeOfDiscount.NEW_USER).getDiscount());
        user.setPassword(Coder.hashmd5("qwerty"));
        user.setName("Елена");
        user.setLastName("Минич");
        user.setAdmin(false);
        user.setActive(true);
        user.setVisits(0);
        user.setId(Math.abs(user.hashCode()));

        User newUser = userDAO.findEntityById(user.getId());
        Assert.assertEquals(newUser.getId(), user.getId());
        Assert.assertEquals(newUser.getEmail(), user.getEmail());
        Assert.assertEquals(newUser.getPassword(), user.getPassword());
        Assert.assertEquals(newUser.getDiscount(), user.getDiscount(),0.01);
        Assert.assertEquals(newUser.getLastName(), user.getLastName());
        Assert.assertEquals(newUser.getName(), user.getName());
        Assert.assertEquals(newUser.isActive(), user.isActive());
        Assert.assertEquals(newUser.isAdmin(), user.isAdmin());
        Assert.assertEquals(newUser.getVisits(), user.getVisits());
    }
    @Test
    public void testUpdateDiscount() throws DAOException {
        logger.info("testUpdateDiscount");
        User user = new User();
        user.setEmail("pupka@gmail.com");
        user.setDiscount(DiscountDAOImpl.getInstance().findDiscountByType(TypeOfDiscount.NEW_USER).getDiscount());
        user.setPassword(Coder.hashmd5("qwerty"));
        user.setName("Елена");
        user.setLastName("Минич");
        user.setAdmin(false);
        user.setActive(true);
        user.setVisits(0);
        user.setId(Math.abs(user.hashCode()));

        userDAO.updateDiscount(user.getId(), TypeOfDiscount.TWO_VISITS);
        User newUser = userDAO.findEntityById(user.getId());
        Assert.assertEquals(newUser.getDiscount(),0.05 ,0.0001);
    }
    @Test
    public void testChangeUserStaus() throws DAOException {
        logger.info("testChangeUserStaus");
        User user = new User();
        user.setEmail("pupka@gmail.com");
        user.setDiscount(DiscountDAOImpl.getInstance().findDiscountByType(TypeOfDiscount.NEW_USER).getDiscount());
        user.setPassword(Coder.hashmd5("qwerty"));
        user.setName("Елена");
        user.setLastName("Минич");
        user.setAdmin(false);
        user.setActive(true);
        user.setVisits(0);
        user.setId(Math.abs(user.hashCode()));

        userDAO.changeStatus(user.getEmail(), false);
        User newUser = userDAO.findEntityById(user.getId());
        Assert.assertEquals(user.isActive(), !newUser.isActive());
    }
    @Test
    public void testChangeVisits() throws DAOException {
        logger.info("testChangeUserVisits");
        User user = new User();
        user.setEmail("pupka@gmail.com");
        user.setDiscount(DiscountDAOImpl.getInstance().findDiscountByType(TypeOfDiscount.NEW_USER).getDiscount());
        user.setPassword(Coder.hashmd5("qwerty"));
        user.setName("Елена");
        user.setLastName("Минич");
        user.setAdmin(false);
        user.setActive(true);
        user.setVisits(0);
        user.setId(Math.abs(user.hashCode()));

        userDAO.changeVisits(user);

        User newUser = userDAO.findEntityById(user.getId());
        Assert.assertEquals(user.getVisits(), newUser.getVisits() - 1);
    }
    @Test
    @After
    public void testDeleteUser() throws DAOException {
        logger.info("testDeleteUser");
        User user = new User();
        user.setEmail("pupka@gmail.com");
        user.setDiscount(DiscountDAOImpl.getInstance().findDiscountByType(TypeOfDiscount.NEW_USER).getDiscount());
        user.setPassword(Coder.hashmd5("qwerty"));
        user.setName("Елена");
        user.setLastName("Минич");
        user.setAdmin(false);
        user.setActive(true);
        user.setVisits(0);
        user.setId(Math.abs(user.hashCode()));

        userDAO.delete(user.getId());

        User newUser = userDAO.findEntityById(user.getId());
        Assert.assertEquals(null,newUser);
    }
    @AfterClass
    public static void destroyPool() {
        try {
            DBConnectionPool.getInstance().destroyConnectionPool();
        } catch (ConnectionPoolException e) {
            logger.error(e);
        }
    }

}