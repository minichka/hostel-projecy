package by.epam.hostel.util;

import by.epam.hostel.resource.MessageManager;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by NotePad.by on 30.09.2016.
 */
public class ValidatorTest {
    @org.junit.Test
    public void orderDate() throws Exception {
        Validator validator = Validator.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = simpleDateFormat.parse("2017-04-12");
        Date date2 = simpleDateFormat.parse("2014-03-12");
        assertEquals(validator.orderDate(date1,date2), MessageManager.ORDER_DATE_DEPARTURE_ERROR);
        Date date3 = simpleDateFormat.parse("2014-03-12");
        assertEquals(validator.orderDate(date3,date2), MessageManager.ORDER_DATE_ARRIVAL_ERROR);
    }

}